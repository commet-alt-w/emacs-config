# emacs-config

emacs config

## prerequisites

1. install tools via system package manager
   - to compile pdf-tools
     - `gcc g++ make automake autoconf`
   - for spell checking
     - `hunspell-en`
   - for git repo interaction
     - `git`
   - for shell script syntax checking
     - `shellcheck`
   - for markdown view
     - `npm`
     - install marked using npm
       1. configure in shell rc file i.e. `~/.bashrc`
          ``` shell
          export NPM_PACKAGES="$HOME/.npm-packages"
          export PATH="$PATH:$NPM_PACKAGES/bin:$HOME/.local/lib"
          ```
       1. source shell rc i.e.
          - `$ source ~/.bashrc`
       1. install marked
          - `$ npm install marked`
1. clone git repo
   - `$ git clone https://gitlab.com/commet-alt-w/emacs-config`
   - remember the path to where you cloned the repo
1. create symbolic link for `~/.emacs.d`
   - `$ ln -s <path/to/emacs-config>/.emacs.d ~/.emacs.d`

## on first run

1. open init.el file
   - `C-x C-f ~/.emacs.d/init.el`
1. optional
   - install the b-mode.el file in the src folder
     - `M-x package-install-file ~/.emacs.d/src/b-mode.el`
   - install the calculate-lisp-indent.el file in the src folder
     - `M-x package-install-file ~/.emacs.d/src/calculate-lisp-indent.el`
   - install the debug-buffer.el file in the src folder
     - `M-x package-install-file ~/.emacs.d/src/debug-buffer.el`
   - check these files to see what they do
1. install packages
   - this will take some time to download and compile things
   - `M-x install-packages`
1. install icon fonts
   - `M-x all-the-icons-install-fonts`
1. install pdf tools
   - `M-x pdf-tools-install`
1. install rtags server
   - `M-x rtags-install`
1. reload init.el
   - if using org-roam/org-roam-ui
     - this will compile org-roam sqlite database stuff which can take some time
   - `M-x load-file ~/.emacs.d/init.el`

## updates

- don't forget to git pull for repo changes and updates!
  ``` shell
  $ cd <path/to/emacs-config>
  $ git pull
  ```
- checking for new packages
  - `M-x install-packages`
- checking packages for updates
  - `M-x update-packages`

## notes

- keybindings/usage/features
  - emacs
    - [emacs notes](https://gitlab.com/commet-alt-w/emacs-config/-/blob/master/.emacs.d/notes/emacs-notes.md)
  - org-mode/roam
    - [org-mode/roam notes](https://gitlab.com/commet-alt-w/emacs-config/-/blob/master/.emacs.d/notes/org-mode-notes.md)
- elisp code
  - [elisp notes](https://gitlab.com/commet-alt-w/emacs-config/-/tree/master/.emacs.d/notes/elisp)

## license

[gnu gpl v3.0](/license)

