# emacs todo

list of items for emacs todo
specifically focused on effective development environments

## todo

### packages

- [ ] geiser
- [ ] flyspell
- [ ] tramp
- [ ] fix-words
- [ ] hide show
- [ ] ido
- [ ] multiple cursors

### other

- [ ] what is org mode?
  - deep dive into org-mode and org-roam
- [ ] what is dire?
- [ ] can enable-package macro use autoload instead of require?
  - autoload changes behavior or eal
- [ ] emacs daemon
- [ ] dashboard
  - treat dashboard as overlay that doesn't affect window tree
  - maybe save window layout before switching to dashboard then revert layout switching back
