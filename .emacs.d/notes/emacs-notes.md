# emacs notes

notes on emacs usage

## modifier keys

- `C-` = \<ctrl>
- `M-` = \<alt>
- `S-` = \<shift>
- `s-` = \<meta>/\<win>/\<super>

## universal prefix

- add `C-u` before other binding

## config file

- `~/.emacs.d/init.el`
- reload config
  - `M-x load-file`

## save file

- save current buffer
  - `C-x C-s`
- save as
  - `C-x C-w`

## exit emacs

- `C-x C-c`

## quit minibuffer

- `C-g`
- `<esc><esc><esc>`

## evaluate expression

- `M-x eval-expression`
  - `M-:`
- eval-last-expression in current buffer
  - `C-x C-e`
  - useful in scratch buffer

## interactive emacs lisp repl (ielm)

- `M-x ielm`

## customize appearance

- `M-x customize-face`
  - can edit header-line like this
- get face attributes under cursor
  - `C-u C-x =`

## find file/new file

- `C-x C-f`

## directories

- create a directory
  - `M-x create-directory`
  - `M-x make-directory`
- copying directories
  - `M-x copy-directory`
- deleting directories
  - `M-x delete-directory`

## string search

- begin search
  - `C-s`
- next match
  - `C-s`
- previous match
  - `C-r`

## string replace

- `M-x replace-string`

## marking (highlighting) a region

- highlight whole buffer
  - `C-x h`
- highlight region
  - `S-<arrow>`

## copy and paste

- highlight region
  - `S-<arrow>`
- un-highlight 
  - `C-g`
- cut current line from cursor
  - `C-k`
- cut region
  - `C-w`
- copy
  - `M-w`
- paste
  - `C-y`
- highlight whole file
  - `S-<esc>-<`

## undo and redo

- undo/redo
  `C-x u`
  `C-_`

## editting multiple lines

1. `S-<arrow>` to highlight region
1. enter new text to rectangle
   - `C-x r t`
1. delete a rectangle
   - `C-x r d`

## windows

- close window
  - `C-x 0`
- close all other windows
  - `C-x 1`

## buffers

- split buffer horizontally
  - `C-x 2`
- split buffer vertically
  - `C-x 3`
- return to only one buffer
  - `C-x 1`
- switch buffer
  - `C-x o`
  - `M-x switch-to-buffer`
  - `C-x b`
- switch buffer to another open buffer
  - `C-x b`
- kill current buffer
  - `C-x k`
- cycle buffers
  - `C-x <arrow>` (left/right arrow)
- move cursor to top of buffer (beginning of file)
  - `<esc> <`
- more cursor to bottom of buffer (end of file)
  - `<esc> >`
- set goal column at cursor pos
  - `C-x C-n`
- cancel goal column
  - `C-u C-x C-n`
- refresh buffer
  - `C-x C-v <RET>`

## changing case

- convert region to lower-case
  - `C-x C-l`
- convert region to upper-case
  - `C-x C-u`

## read-only buffers

- toggle read-only mode
  - `C-x C-q`
      
## desktop mode

- save current session
  - `M-x desktop-save`

## info pages

- show emacs info pages
  - `C-h i`
- show commands
  - `C-h m`
- open file in info mode
  - `C-u C-h i`
      
## functions, commands, properties

- searching for functions, commands, properties
  - `- M-x apropos <RET> buffer erase`

## horizontal scrolling

- scroll left
  - `C-x <`
- scroll right
  - `C-x >`
- notes: needs to be enabled in init.el

## man pages

- show man page for a given function
  - `M-x manual-entry`

## help

- `C-h ?`

## describe

- variable
 - `C-h v <variable>`
- key
  - `M-x describe-key`
- key binding for command
  - `C-h w <command>`
- bindings
  - show all key bindings
    - `M-x describe-bindings`
  - describe mode
    - `C-h m`
  - describe binding
    - `C-h c`
- package
  - `C-h P`
- function
  - `C-h f`

## packages

- describe package (search package)
  - describe package
    - `C-h P`
  - search by keyword
    - `C-h p`

## commenting

1. `S-<arrow>` to higlight region
1. `M-x comment-region`
1. `M-x uncomment-region`
      
## indent

- indent region
  - `C-M-\`

## packages

- list installable
  - `M-x list-packages`

## shell commands

- execute in current buffers default directory
- shell-command
  - `M-!`
  - `M-x shell-command`
- async 
  - `M-&`
  - `M-x async-shell-command`
- eshell
  - `M-x eshell`
  - spwan more eshells with universal prefix
    - `C-u M-x eshell`
  - syntax
    - `(...)`
      - s-expressions
    - `${...}`
      - cli commands
  - beware: a lot of stuff does not work in eshell
    - test outside of eshell if you think something is wrong

## fonts

- `(set-face-attribute 'default t :font "...")`
  - `(set-frame-font "..." nil t)`
    - no idea what these do, don't seem to work at all
- font size
  - `(set-face-attribute 'default nil :height 100)`
    - works fine
- in .xresources
  - `Emacs.font: ...`
    - actually changes font
- `...` == font name that shows up in `:) fc-list`
    
## narrow-to-region

- isolate code/region in buffer
  1. higlight region
  2. `C-x n n`
     - or `M-x narrow-to-region`
- make whole buffer visible again
  `C-x n w`

## diary

keep track of appointments or other events on a daily basis

- diary file location
  `diary-file`

### diary file

- format
  ```
  12/22/2015  Twentieth wedding anniversary!
  10/22       Ruth's birthday.
  * 21, *:    Payday
  Tuesday--weekly meeting with grad students at 10am
           Supowit, Shen, Bitner, and Kapoor to attend.
  1/13/89     Friday the thirteenth!!
  thu 4pm     squash game with Lloyd.
  mar 16      Dad's birthday
  April 15, 2016 Income tax due.
  * 15        time cards due.
  ```
  - each entry
    - describes one event
    - consists of one or more lines
    - begins with date
  - multiple lines
    - an entries extra lines must be indented
  - exclusion
    - entries that do not start with a date
    - entries that do not start with a valid date
- daily task format
  `* * 10:00 this is a daily task at 10am`
- single entry display format
  ```
  02/11/2012
      Bill B. visits Princeton today
      2pm Cognitive Studies Committee meeting
      2:30-5:30 Liz at Lawrenceville
      4:00pm Dentist appt
      7:30pm Dinner at George's
      8:00-10:00pm concert
  ```
  - has different appearance when using simple diary display

### displaying the diary

- calendar mode
  - `M-x calendar`

### dates

- styles
  - `calendar-date-style`
  - american
    - `month, day, year`
  - european
    - `day, month, year`
  - iso
    - `year, month, day`
- diary format
  - `month/day`
  - `year/month/day`
  - `day`
  - `*/*/*`
- diary format examples
  ```
  4/20/12  Switch-over to new tabulation system
  apr. 25  Start tabulating annual results
  4/30  Results for April are due
  */25  Monthly cycle finishes
  Friday  Don't leave without backing up files
  * * 00:00 everyday at 00:00
  ```

### adding to diary

- in calendar mode
  - add entry for selected date
    - `i d`
  - add entry for selected week
    - `i w`
  - add entry for selected month
    - `i m`
  - add entry for selected year
    - `i y`

### special entries

- written as lisp expression
  - `%%(diary-block 2022-10-31) halloween`
- add anniversary
  - `i a`
- add block entry
  - `i b`
- add cyclic diary entry starting at date
  - `i c`
- floating diary entry: regularly occuring events
  - `month day-of-week repeat`
  - `&%%(diary-float 11 4 4)`
    - american thanks giving
  - `%%(diary-float t 1 1) trash day`
    - first day of every month

### appointments

- enable appointment notification
  - `M-x appt-activate`
- appointment format
  ```
  Monday
    9:30am Coffee break
   12:00pm Lunch
  ```
- specify (in minutes), how long before to be notificed
  `appt-message-warning-time`
- time format
  - am/pm
  - 24 hr
- alarm clock
  - `M-x appt-add`
    - does not affect diary file
  - `M-x appt-del`

## calendar

- starting calendar
  - `M-x calendar`
- commands
  - show org agenda for the day
    - `c`
  - insert new diary entry
    - `i`
  - show phases of the moon
    - `M`
  - show sunrise/sunset times
    - `S`
  - convert date at point to othe cultural/historic standards
    - `C`
  - show holidays for 3 months around point
    - `H`
  - mark holidays
    - `x`
  - show diary entries for that day
    - `s`
  - exit
    - `q`

## daemon

### starting/stopping daemon

- run emacs in daemon mode
  - `$ emacs --daemon`
    - add to init script or wm config
  - `M-x server-start`
  - foreground daemon
    - `$ emacs --fg-daemon`
- stop emacs daemon
  - from inside emacs
    - `M-x save-buffers-kill-emacs`
  - from outside of emacs
    - `$ emacsclient -e '(save-buffers-kill-emacs)'`
- useful shell alias'
  - can get annoying to start/stop daemon
  ``` shell
  alias emacsd="emacs --daemon &>/dev/null &"
  alias emacsdstop="emacsclient -e '(save-buffers-kill-emacs)'"
  ```

### connecting to daemon

- starting emacsclient
  - gui
    - `$ emacsclient -c --eval '(load-file "~/.emacs.d/init.el")'`
      - eval statement breaks if emacs daemon is not already started
    - `$ emacsclient -c`
  - tui
    - `$ emacsclient -t`
  - desktop file
    - `~/.local/share/applications/emacsclient.desktop`
    ``` shell
    [Desktop Entry]
    Name=Emacs (Client)
    GenericName=Text Editor
    Comment=Edit text
    MimeType=text/english;text/plain;text/x-makefile;text/x-c++hdr;text/x-c++src;text/x-chdr;text/x-csrc;text/x-java;text/x-moc;text/x-pascal;text/x-tcl;text/x-tex;application/x-shellscript;text/x-c;text/x-c++;
    Exec=emacsclient -c %F
    Icon=emacs
    Type=Application
    Terminal=false
    Categories=Development;TextEditor;Utility;
    StartupWMClass=Emacs
    ```
  - shell variables
    ``` shell
    export EDITOR="emacsclient -t"
    export VISUAL="emacsclient -c"
    ```
- close frame
  - `C-x 5 0`
- styles & ui element customizations for frames
  - `$ emacsclient` cli command starts an emacs frame
    - emacs daemon loads styles, fonts, ui customizations before they can be set on frame
  - reload init.el file when starting a new frame
    - from outside of emacs
      - `$ emacsclient ... -e '(load-file ".../init.el")'`
    - from inside of emacs
      ``` elisp
      (add-hook 'server-after-make-frame-hook (lambda () (load-file "~/.emacs.d/init.el")))
      ```
      - or create a funcallable to make style/ui customizations and pass it through the lambda

## navigation

- return to previous position in file
  - `C-u C-<spc>`

## mark

- set mark
  - `C-<spc>`

## debugging

- start emacs in quick mode (no init file)
  - `emacs --quick`

## elisp

- [elisp notes](https://gitlab.com/commet-alt-w/emacs-config/-/tree/master/.emacs.d/notes/elisp)
- debug buffers
  ``` elisp
  (setq debug-buffer (get-buffer-create "*debug*"))
  (switch-to-buffer debug-buffer)
  (print "hi" debug-buffer)
  ```
- evaluating an entire elisp file
  - `M-x load-file`

### debugging elisp

- get full backtrace
  - `M-x toggle-debug-on-error`
- quit backtrace
  - `q`
- check parens
  - `M-x check-parens`
- enable parens mode
  - `M-x show-paren-mode`
- find unbalanced parens
  - `C-u C-M-u`
- move forward over parens
  - `C-M-n`
- move backward over parens
  - `C-M-p`
- move up over parens
  - `C-M-u`
- move down over parens
  - `C-M-d`
- navigate by balanced parens
  - `C-M-f`/`C-M-b`
- indents
  - `TAB`
