;; intoduction to emacs lisp
;; https://www.youtube.com/watch?v=RQK_DaaX34Q

;; data list
(setq somelist '(1 2 3 4 5))

;; create debug buffer and switch to it
(setq buffername "*debug*")
(setq debug-buffer (get-buffer-create buffername))
(switch-to-buffer debug-buffer)

;;;; car, cdr, cons, nthcdr, cadr

;; car returns the first item in a list)
(car somelist)

;; cdr remove first item from a list
(cdr somelist)

;; returns a new list adding an
;; atom to the head of a list
;; adds a new "car"
(cons '1 '(2 3 4 5))

;; length of a list
(length (cons '1 '(2 3 4 5)))

;; nthcdr
;; removes n amount of cars from a list
(nthcdr 2 somelist)

;; cadr returns the second item in a list
(cadr somelist)

;;;; association list

;; key value pairs
(setq some-alist '(numbers
                   (1 . "one")
                   (2 . "two")
                   (3 . "three")))

;; query association list given a key
(assq '1 some-alist)

;; understanding package update funcallable
(dolist (package package-list)
  (setq current-version
        (package-desc-version (cadr (assq package package-alist))))
  (setq repo-version
        (package-desc-version (cadr (assq package package-archive-contents))))
  (princ current-version debug-buffer)
  (print repo-version debug-buffer)
  (unless (version-list-= repo-version current-version)
    (print "not equal" debug-buffer)))
