;; defining functions and commands
;; https://www.youtube.com/watch?v=EqgkAUHw0Yc

;;;; functions
;; reusable piece of code
;; accepts inputs via parameters
;; usually returns a result
;; can be named or can be anonymous (lambda)
;; can be called by other code or functions

;; defining functions

(defun do-some-math (x y)
  (* (+ x 20)
     (- y 10)))

(do-some-math 100 50)

;; function args
;; optional arguments: can be provided but not required
;; rest arguments: one variable contain an arbitrary amount of remaining parameters

(defun multiply-maybe (x &optional y z)
  (* x
     (or y 1)
     (or z 1)))

(multiply-maybe 5)
(multiply-maybe 5 2)
(multiply-maybe 5 2 10)
(multiply-maybe 5 nil 10)
(multiply-maybe 5 2 10 7)

(defun multiply-many (x &rest operands)
  (dolist (operand operands)
    (when operand
      (setq x (* x operand))))
  x)

(multiply-many 5)
(multiply-many 5 2)
(multiply-many 5 nil 10)
(multiply-many 5 2 10 7)

(defun multiply-two-or-many (x &optional y &rest operands)
  (setq x (* x (or y 1)))
  (dolist (operand operands)
    (when operands
      (setq x (* x operand))))
  x)

(multiply-two-or-many 5)
(multiply-two-or-many 5 2)
(multiply-two-or-many 5 2 10 7)

;; documenting functions
;; first form in a function body can be a string which describes the function

(defun some-function (x y)
  "first form, string defining the function"
  (* x y))

;; anonymous functions
;; lambdas !

(lambda (x y)
  (+ 100 x y))

;; can invoke lambdas directly
((lambda (x y)
   (+ 100 x y))
 10 20)

;;;; invoking functions

;; regular way
(+ 2 2)

;; call it by symbol
(funcall '+ 2 2)

;; define a functions that accepts a function
(defun gimmie-function (fun x)
  (message "function: %s -- result: %d"
           fun
           (funcall fun x)))

;; store a lambda in a variable
(setq lambda-var (lambda (arg) (+ arg 1)))
(defun named-version (arg)
  (+ arg 1))
(gimmie-function (lambda (arg) (+ arg 1)) 5)
(gimmie-function lambda-var 5)
(gimmie-function 'named-version 5)

;; can use apply to pass list of values ot a function
(apply '+ '(2 2))
(funcall '+ 2 2)
(apply 'multiply-many '(1 2 3 4 5))
(apply 'multiply-two-or-many '(1 2 3 4 5))

;;;; defining commands

;; commands that show up using `M-x ...`
;; meant to be invoked directly by the user
;; can be invoked using key-bindings
;; can have params sent via prefix arguments, `C-u`
;; commands are interactive functions

;; defining an interactive function

(defun some-command ()
  "some command"
  (interactive)
  (message "hey, it's a command"))

;; interactive parameters
;; `interactive` form accepts parameters
;; tells emacs what to do when the command is executed interactively

;; general args
;; N - prompt for numbers
;; p - user numeric prefix without prompting
;; M - prompt for string
;; i - skip an irrelevant arg

;; fils, directories, buffers
;; F - prompt for file
;; D - prompt for directory
;; b - prompt for a buffer

;; functions, commands, vars
;; C - prompt for command
;; a - prompt for function name
;; v - prompt for custom variable name

;; i.e.

(defun do-some-math-again (x y)
  "do some math"
  (interactive "Nenter a vlue for x: \nNy: ")
  (* (+ x 20)
     (- y 10)))

(defun ask-favorite-fruit (fruit-name)
  (interactive "Menter favorite fruit: ")
  (message "fruit name: %s" fruit-name))

