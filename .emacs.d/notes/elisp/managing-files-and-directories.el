;; managing files and directories in practice
;; https://systemcrafters.cc/learning-emacs-lisp/managing-files-and-directories/

;; vars
(setq this-file "managing-files-and-directories.el")
(setq init-el-file "$HOME/.emacs.d/init.el")

;; what is a symbolic link?
;;  entry in the file system which points to a file or directory
;;  somewhere else in the file system

;; getting the current directory
(setq current-directory default-directory)

;;;; manipulating file paths

;; emacs implements files in two parts
;;  1. directory
;;  2. filename and extension

;; file-name functions
(file-name-directory (buffer-file-name))
(file-name-nondirectory (buffer-file-name))
(file-name-extension (buffer-file-name))
(file-name-sans-extension (buffer-file-name))
(file-name-base (buffer-file-name))
(file-name-as-directory (buffer-file-name))
(file-name-as-directory
 (file-name-sans-extension (buffer-file-name)))

;;;; resolving file paths

(file-name-absolute-p (buffer-file-name))
(file-name-absolute-p this-file)

(file-relative-name (buffer-file-name))

(expand-file-name this-file)

;; won't substitute environment variables
(expand-file-name init-el-file)
;; but this will substitute environment variables
(substitute-in-file-name init-el-file) 

;;;; checking if files and directories exist

(file-exists-p (substitute-in-file-name init-el-file))

;; can also check if a user has access to a file, if it's writebale/eecutable
(file-readable-p (substitute-in-file-name init-el-file))
(file-executable-p (substitute-in-file-name init-el-file))
(file-writable-p (substitute-in-file-name init-el-file))

;;;; creating directories

;; (make-directory "path" t/nil)
;; create directories
;; use optional argument to create parent directories
;; won't throw error if directory exists if its set to t as well
(make-directory "~/.emacs.d")
(make-directory "~/.emacs.d" t)

;;;; listing files in a directory

(directory-files "~/.emacs.d")

;; return full file paths
(directory-files "~/.emacs.d" t)
;; get all fils containing a specific extensions
(directory-files current-directory t ".el")
;; don't sort results
(directory-files current-directory t "" t)

;; can search directories recursively
(directory-files-recursively "~/projects/learning/emacs/emacs-source" "\\.el")

;;;; copying, moving, and deleting files and directories

;; (copy-file ...)
;; (copy-directory ...)
;; (rename-file ...)
;; (delete-file ...)
;; (delete-directory ...)

;;;; creating symbolic links

;; (make-symbolic-link ...)

;; can also check for symbolic links
;; (file-symlink-p ...)
;; (file-truename ...)
