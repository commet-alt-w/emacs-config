;; creating a custom minor mode
;; https://systemcrafters.cc/learning-emacs-lisp/creating-minor-modes

;; what is a mode?

;; major mode
;;  main mode for a buffer, can only be one active at a time
;;  `M-x org-mode`
;; minor mode
;;  provides supporting functionality either in a sepcific buffer
;;  or globally in emacs
;;  `(auto-revert-mode 1)`

;;;; writing a basic minor mode

;; conventions
;;  : a variable that ends in `-mode` like `b-mode`
;;  : command for enabling and disabling the mode
;;  : optional keymap
;;  : optional lighter

;; may want a hook for a minor mode
;;   hooks are variables that store a list of functions
;;   to be invoked when something happens, like activation

(defvar b-mode-hook nil
  "hook for b-mode")

;; can execute hooks with
(run-hooks 'b-mode-hook)

;; must add a function to the hook
(add-hook 'b-mode-hook
          (lambda ()
            (message "hook executed")))

;; defining a basic minor mode
(make-variable-buffer-local
 (defvar b-mode nil
   "toggle b-mode"))

(defvar b-mode-map (make-sparse-keymap)
  "keymap for b-mode")

(define-key b-mode-map (kbd "C-c C-b")
  (lambda ()
    (interactive)
    (message "b-mode key binding used")))

(add-to-list 'minor-mode-alist '(b-mode " b-mode"))
(add-to-list 'minor-mode-map-alist (cons 'b-mode b-mode-map))

(defun b-mode (&optional ARG)
  (interactive)
  (setq b-mode
        (if (eq ARG 'toggle)
            (not b-mode)
          (> ARG 0)))
  (if b-mode
      (message "b-mode active")
    (message "b-mode not active"))
  (run-hooks 'b-mode-hook))

;;;; define-minor-mode macro

;; makes it easier to define a minor mode
(define-minor-mode b-mode
  "toggles global b-mode"
  ;; initial value
  nil
  :global t
  :group b
  :lighter " b-mode"
  :keymap
  (list (cons (kbd "C-c C-b")
              (lambda ()
                (interactive)
                (message "b-mode key binding used"))))
  (if b-mode
      (message "b-mode activated")
    (message "b-mode not active")))

(add-hook 'b-mode-hook (lambda ()
                         (message "b-mode-hook executed")))
