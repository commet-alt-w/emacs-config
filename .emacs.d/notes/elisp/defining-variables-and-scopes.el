;; definining variables and scopes
;; https://www.youtube.com/watch?v=tq4kTNL1VD8

;; what is a variable?
;; an association (binding) between a name (symbol) and a value

;; setting variables
;; setq is a convenience function for setting variable bindings
;; removes the need for quoting a symbol name
(setq some-var 4)
(set 'some-ther-var 4)

;; can set multiple variables in one expression
(setq some-var 5
      some-other-var 5)

;;;; defining variables

;; can document variables with defvar
;; defvar only applies value binding if the binding doesn't exist
;; can use defvar to set values before package gets loaded
;;   and settings will not be overridden 
(defvar documented-var "documented"
  "a documented variable")

;;;; buffer local variables

;; can set values for the current buffer only
(setq-local some-var 5)

;; can also make variables local for all buffers
(setq some-var t)
(make-variable-buffer-local 'some-var)

;; when writing emacs lisp package, use defvar
(defvar new-buffer-local-var 311
  "311 buffer local var")
(make-variable-buffer-local 'new-buffer-local-var)

;; can set default values for buffer local variables
;; will create value that doesn't exist in the current buffer, only future buffers
(setq-default some-var nil)

;;;; defining variable scopes

;; scope is a region of code where a variable i sbound to a particular value (or not)
;; value of any variable can be different depending on where you access it

;; global scope
;; variable is visible to any other code
;; buffer-local variables are still global, but only for a particular buffer
;; global vars are good for storing config values used by modes and commands
;; or for storing data that needs to be accessed by future invocations of code

;; defining local scope with let
;; local variables override global vars
;; local variables do not affect global vars
(setq x 500)
(defun do-the-loop ()
  (interactive)
  (let ((x 0))
    (message "starting the loop from %d" x)
    (while (< x 5)
      (message "loop index: %d" x)
      (incf x))
    (message "done!")))
(do-the-loop)

;; defining multiple bindings with let and let* 
;; can mind multiple variables in the local scope

(let ((y 5)
      (z 10))
  (* y z))

;; let* allows to refer to refer to variables within the declaration

(let ((y  5)
      (z (+ y 5))) ;; canot refer to y here, throws error
  (* y z))

(let* ((y 5)
       (z (+ y 5))) ;; this works with let*
  (* y z))

;;;; dynamic scope

;; default scope
;; value associated with a variable may change depending on
;;   where an expression gets evaluated

(setq x 5)

(defun do-some-math (y)
  (+ x y))

(let ((x 15))
  (do-some-math 10)) ;; 25

(do-some-math 10) ;; 15

;;;; defining customization variables

;; customizable variables are used to define user-facing settings
;;  for customization and behavior of emacs and packages
;; these variables show up in customization UI
;;  and users can change them without code

(defcustom my-custom-variable 42
  "a customizable variable")

;; defcustom can have parameters

;; :type - expected value type
;; :group - group defined by defgroup
;; :options - list of possible values
;; :set - function to be invoked when this variable is customized
;; :get - function to be invoked when this variable is resolved
;; :initlize - function to initialize the variable when its defined
;; :local - when t, marks variable as buffer-local

;; setting customizable variables correctly

;; defcustom's :set is not triggered when a variable
;;  is changed with setq, use customize-set-variable
(customize-set-variable 'tab-width 2)

;; `M-x describe-variable` can let you know if a variable is cuztomizable
