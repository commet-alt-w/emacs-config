;; reading and writing buffers in practice
;; https://www.youtube.com/watch?v=J7d2LmivyyM

;; buffers can do a lot
;; automate the editing or creation of files
;; create a custom display for information for a major mode
;; communicate with external programs
;; provide interactive interfaces like REPLs and more

;; what is a buffer?
;;   an object that contains text to be displayed, edited, or manipulated
;;   content of buffer might not come from a file, could be generated
;;   text in emacs has properties: font, color, size, etc.
;;   usually displayed by a window, can work with buffers without displaying them tho

;; getting the current buffer
(current-buffer)

;; getting a buffer by name
(get-buffer "*scratch*")

;; create buffers
(get-buffer-create "*debug*")

;; changing the current buffer
(progn
  (set-buffer (get-buffer "*scratch*"))
  (current-buffer))

;; changing the current buffer safely
(progn
  (save-current-buffer
    (set-buffer "*scratch*")
    (message "current buffer: %s" (current-buffer)))
  (current-buffer))

(progn
  (with-current-buffer "*scratch*"
    (message "current-buffer: %s" (current-buffer)))
  (current-buffer))

;;;; working with file buffers

;; most buffers in emacs are text loaded from a file

;; can use buffer-file-name to get full path of file
(buffer-file-name)

;; can also get a buffer that represents a specific file or path
(get-file-buffer "reading-and-writing-buffers.el")

;; loading a file into a buffer
;; will always return the same buffer
;; may prompt user if buffer for the file is modified and not saved
;;  can send t as an argument to prevent prompt
;; use this function to create a buffer for a file if it doesn't exist
(find-file-noselect "reading-and-writing-buffers.el")

;;;; point

;; location of cursor within the buffer
(point)
;; can also get min/max point locations of the buffer
(point-min)
(point-max)

;; can move the point

;; goto-char - move point to a specific position (integer)
;; forward-char - move point forward by a number of positions
;; backward-char - move the point backward by a number of positions
;; beginning-of-buffer - go to beginning of buffer
;; end-of-buffer - go to end of buffer
;; forward-word - move forward by one word
;; backward-word - move backward by one word

;; can save current point location
(save-excursion
  (goto-char (point-max))
  (point))

;; examining buffer text

(char-after)
(char-after (point))
(char-after (point-min))

;; can get a substring
(buffer-substring (point-min) (+ 10 (point-min)))

;; thing-at-point
;;   can grab text at the point if it matches a specific type
(thing-at-point 'word)
(thing-at-point 'sentence)
(thing-at-point 'sentence t)
(thing-at-point 'sexp)

;; searching for text
(search-forward "ways")
(search-backward "foward")
(search-backward "backward" nil t 1)
(search-backward "backward" nil t 3)

;; inserting text
(insert " ^_^")
(insert "\n" "this is" ?\s ?\n "something else")
(insert-char ?\- 20)

;; deleting text
(with-current-buffer "somebuffer"
  (delete-region (point) (point-max)))

;; saving a buffer
(save-buffer)
