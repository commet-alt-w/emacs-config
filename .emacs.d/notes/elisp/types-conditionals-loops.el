;; types, conditionals, and loops
;; https://www.youtube.com/watch?v=XXpgzyeYh_4

;;;; true and false

;; t is true
;; nil is false

;; these are symbols in elisp
(type-of t)
(type-of nil)

(if t
    (message "true"))

;;;; equality

(setq testval '(1 2 3))

;; eq - are two objects the same?
(eq 1 1) 
(eq "thing" "thing")
(eq '(1 2 3) '(1 2 3))
(eq testval testval)

;; eql - are two parameters the same object or same number?

;; equal - are the two parameters equivalent 

;;;; numbers

;; integers
1
;; floats
3.14

;;;; operations

(+ 5 5)
;; can only do this syntax with 1?
(1+ 5)

;; truncate - rounds float to integer by moving towards 0
(truncate 1.2)
(truncate -1.2)

;; floor - rounds float to integer by subtracting
(floor 1.2)
(floor -1.2)

;; ceiling - round up to the next integer
(ceiling 1.2)
(ceiling 1.0)

;; round - round to the nearest integer
(round 1.5)
(round 1.4)

;; predicates - identify number types
(integerp 1)
(integerp 1.2)
(floatp 1)
(floatp 1.2)
(numberp 1)
(numberp 1.2)
(zerop 1)
(zerop 0)
(zerop 0.0)

;;;; characters

;; chars are integers interpreted as characters
?A
?\n
?\N{U+E0}
?\C-c
(kbd "M-x")

;; predicates
(characterp ?b)

;; comparisons
(char-equal ?B ?B)
(char-equal ?B ?b)

;;;; sequences

;; lists, strings, arrays are all sequences

(sequencep "test")
(sequencep "")
(sequencep [1 2 3])
(sequencep ?b)
(sequencep 22)
;; nil is an empty sequence 
(sequencep nil)

(length "hello")
(length '(1 2 3))
(length nil)

;; sequences are 0 based indexed
(elt "hello" 1)
(elt '(3 2 1) 2)

;;;; strings

;; arrays of characters
"hello"
"hello \
 self"
;; can escape \ with \
"hello \\ self"

(make-string 5 ?!)
(string ?h ?e ?l ?l ?o ?!)

;; predicates
(stringp "test")
(stringp 1)
(stringp nil)
(stringp ?b)
(string-or-null-p nil)

(arrayp "array")
(listp "list")

;; comparisons

(string= "hello" "hello")
(string< "bhello" "hello")
(string> "bhello" "hello")

;; operations
(substring "hello" 0 4)
(substring "hello" 1)
(concat "hello " "self" "!")
(concat)
(setq somestring "hello self!")
(split-string somestring)
(split-string somestring "e")
(split-string somestring "[ !]")
(split-string somestring "[ !]" t)
;; what is case-fold-search?

;; formatting
(format "hello %d %s!" 100 "self")
(format "here's a list: %s" `(1 2 3))

;; messages
(message "this is %d" 5)

;;;; lists

;; cons cells
;; lists are built out of cons cells
;; cons cells enable to chain together lists together
;; using the cons container
;; cons are like a pair or a tuple with values
;; cons cells can be accessed with car and cdr

;; car - get first value in the cons
;; cdr - get second value in the cons

(cons 1 2)
'(1 . 2)

(assq '1 '(somelist (1 . 2) (2 . 3) (3 . 4)))

(setq some-alist (cons 1 2))
(car some-alist)
(cdr some-alist)
(setcar some-alist 3)
(setcdr some-alist 4)
some-alist

;; building lists from cons cells
(cons 1 (cons 2 (cons 3 (cons 4 nil))))
(cons 1 '(2 3 4 5))

(cons '(1 2 3) '(4 5))

(append '(1 2 3) 4)
(append '(1 2 3) '(4))

;; predicates
(listp '(1 2 3))
(listp 1)
(cons 1 nil)
(append '(1) nil)

(listp (cons 1 2))
(consp (cons 1 (cons 2 (cons 3 nil))))

;; alists

;; assosociation lists
;; lists containing cons pairs
;; key-value maps

(setq some-alist
      '((one . 1)
        (two . 2)
        (three . 3)))

(alist-get 'one some-alist)
(alist-get 'four some-alist)

;; retrieve cons cell given some key
(assq 'one some-alist)
;; retrieve cons cell given some value
(rassq 1 some-alist)

;;;; property lists (plist)

;; another key/value pairs with a flat list
(setq some-plist '(one 1 two 2))
(plist-get '(one 1 two 2) 'one)
(plist-get some-plist 'one)
(plist-put some-plist 'three 3)
(plist-get some-plist 'three)

;;;; arrays

;; arrays are fast, arranged contiguously in memory
;; vectors are arrays, strings are also arrays
(setq some-array [1 2 3 4 5])
(aset some-array 1 5)
some-array

(setq some-string "hello!")
(aset some-string 0 ?b)
some-string

(setq some-array [1 2 3])
(fillarray some-array 6)
some-array

;;;; logic expressions

;; and, or expressions
;; truthiness or falsiness
;; everything besides t and the empty list ='()= is considered t

(if t 'true 'false)
(if 5 'true 'false)
(if "Emacs" 'true 'false)
(if "" 'true 'false)
(if nil 'true 'false)
(if '() 'true 'false)

;; operators

;; not - invertss truthe value
;; and - returns last value if all expressions are true
;; or - returns first value that is truthy (short-circuits)
;; xor - returns the first value that is truthy (doesn't short circuit)

(not nil)
(not t)
(not 3)

(and t t t t 'foo)
(and t t t 'foo t)
(and 1 2 3 4 5)
(and nil 'something)

(or nil 'something)
(or nil 'something t)
(or (- 3 3) (+ 2 0))

;;;; conditional expressions

;; if expression evaluates based on the result
;; picks one of two branches to evaluate

(if t 5
  (message "hello")
  (+ 3 3))

;; use progn to enable multiple expressions in the true branch

(if t
    (progn
      (message "true")
      5)
  (message "hello")
  (+ 3 3))

;; if statements are expressions
;; expressions return the value of the last form evaluated inside it

(if t 5
  (message "hello")
  (+ 2 2))

;; when and unless

;; when - evaluate forms when the expression evaluates to t
;; unless - evaluate forms when expression evaluates to nil

(when (> 2 1) 'foo)
(unless (> 2 1) 'foo)

;; both can contain multipple forms and return the reuslt of the last form

(when (> 2 1)
  (message "true")
  (- 5 2)
  (+ 2 2))

(unless (> 1 2)
  (message "true")
  (- 5 2)
  (+ 2 2))

;; cond - multiple condition check, execute resulting forms
;; like a switch statement

(setq a 1)
(setq a 2)
(setq a -1)

(cond ((eql a 1) "equal to 1")
      ((> a 1) "greater than 1")
      (t "something else!"))

;;;; loops

;; while

(setq counter 0)
(while (< counter 5)
  (message "loop: %d" counter)
  (setq counter (1+ counter)))

;; dotimes

(dotimes (count 5)
  (message "loop: %d" count))

;; dolist

(dolist (item '(1 2 3 4 5))
  (message "item: %d" item))

;; recursion

(defun efs/recursion-test (counter limit)
  (when (< counter limit)
    (message "recursion: %d" counter)
    (efs/recursion-test (1+ counter) limit)))

(efs/recursion-test 0 5)
