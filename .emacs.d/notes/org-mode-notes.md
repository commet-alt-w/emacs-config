# org-mode notes

notes on org-mode emacs usage

## general

- restart org-mode for current buffer
  - with point on line starting with `#+`
    - `C-c C-c`
- formatting
  - title
    - `#+title: name`

## document structure

org-mode document structure

### headlines

- headlines/levels
  - `*` `**` `***` etc.
- headline numbering
  - `M-x org-num-mode`
  - `#+startup: num`
- indent
  - `M-x org-indent-mode`
  - `#+STARTUP: indent`
  - `#+STARTUP: noindent`
- properties
  - add property
    - `C-c C-x p`

### footnotes

- create footnote
  ``` elisp
  some text in an entry[fn:1] with a footnote
  ...
  [fn:1] https://orgmode.org
  ```
- footnote action
  - `C-c C-c`
  - `C-c C-x f`

### hyperlinks

- external ilnks
  - define link
    - `[[link][description]]`
    - `[[link]]`
  - escape special characters inside `[link]`
    - all `[` and `]` characters
    - every `\` character preceding either `]` or `[`
    - every `\` character at the end of the link.
- internal links
  - link to `CUSTOM_ID` property
    - `[[#my-custom-id]]`
  - link to headline
    - `[[*Some section]]`
  - link to name keyword
    1. `#+NAME: my entry`
    1. `[[my entry]]`
  - link to target
  ```
  1. one item
  2. <<target>> another item
  refere to item [[target]]
  ```
    - will display the link as the number assinged to target
  - radio targets
    - mark all target text as link
      - `<<<my target>>>`
- protocol types
  - org-mode supports [multiple link protocols](https://orgmode.org/manual/External-Links.html)
  - [link handling](https://orgmode.org/manual/Handling-Links.html)
- link abbreviations
  - `[[linkword:tag][description]]`
  - `#+LINK: duckduckgo https://duckduckgo.com/?q=%s`

### visibility

- cycling states
    - folded -> children -> subtree
- local cycling
   - `<tab>` `S-<tab>`
- global cycling
  - `C-u <tab>`
- switch back to startup visibility
  - `C-u C-u <tab>`
- copy visible text in the region into kill ring
  - `C-c C-x v`
- initial visibility
  - per-file
    ``` elisp
	#+STARTUP: overview
	#+STARTUP: content
	#+STARTUP: showall
	#+STARTUP: show2levels
	#+STARTUP: show3levels
	#+STARTUP: show4levels
	#+STARTUP: show5levels
	#+STARTUP: showeverything
    ```
  - per-entry
    - `:VISIBILITY:`

### motion

- next heading
  - `C-c C-n`
- previous heading
  - `C-c C-p`
- next heading same level
  - `C-c C-f`
- previous heading same level
  - `C-c C-b`
- backward to higher level heading
  - `C-c C-u`
- goto
  - `C-c C-j`
    - tab, down/up, ret, /

### document editing

- turn heading into item
  - `C-c -`
- turn item into heading
  - `C-c *`
- insert new heading/item/row
  - `M-<ret>`
- insert new heading at end of current subtree
  - `C-<ret>`
- insert todo entry
  - `M-S-<ret>`
- insert todo entry at end of current subtress
  - `C-S-<ret>`
- change heading level
  - `M-<left>` `M-<right>`
- change whole subtree level
  - `M-S-<left>` `M-S-<right>`
- move subtree up/down
  - `M-<up>` `M-<down>`
- mark subtree
  - `C-c @`
    - repeat to mark subtree's of the same level
- cut subtree
  - `C-c C-x C-w`
- copy subtree
  - `C-c C-x M-w`
- paste subtree
  - `C-c C-x C-y`
- sort
  - `C-c ^`
- toggle heading
  - `C-c *`

### drawers

- don't display information about an entry
- define drawer
  ``` elisp
  ** heading
  text
  :DRAWERNAME:
  not visible info
  :END
  more text
  ```
- insert drawer
  - `C-c C-x d`
- add a timestamped note to LOGBOOK drawer
  - `C-c C-z`

### emphasis and monospace

- inline code, `~`
  - `~$ ps -efj~`
- bold, `*`
  - `*bold*`
- italic, `/`
  - `/italic/`
- underlined, `_`
  - `_underlined_`
- verbatim, `=`
  - `=varbatim=`
- strike-through, `+`
  - `+strike-through+`

### blocks

- define a block
  ``` elisp
  #+BEGIN
  ...
  #+END
  ```
- source code example
  ``` elisp
  #+BEGIN_SRC emacs-lisp
  (defun org-xor (a b)
    "exclusive or"
    (if a (not b) b))
  #+END_SRC
  ```
- edit special (source code in native environment)
  - `C-c '`
    - type again to exit
- hide/show blocks
  - `#+STARTUP: hideblocks`
  - `#+STARTUP: nohideblocks`
- escape special characters
  - use comma
  - `*`, `#+`
- usage
  - source code examples: literal
  - time logging: [clocking work time](https://orgmode.org/manual/Clocking-Work-Time.html)

### properties

- properties structure
  ``` elisp
  * heading
  :PROPERTIES:
  ...
  :END:
  ```
- properties
  - `:LOGGING: TODO(!) WAIT(!) DONE(!) CANCELED(!) `
  - `:LOGGING: WAIT(@) logrepeat`
  - `:LOGGING: nil`
  - `:NOBLOCKING: t`
  - `:UNNUMBERED: t`
  - `:CATEGORY: music`
  - `:CUSTOM_ID: <id>`
  - `:VISIBILITY:`
    - folded, children, content, all
  - `:COOKIE_DATA: todo recursive`
    - recursive: show statistics for children
    - todo: only count todo items
    - checkbox: only count checkbox items

## tables

### table editor

- insert table
  - `C-c |`
- realign table
  - `C-c C-c`
- move to next/previous field and realign table
  - `<tab>`/`S-<tab>`
- erase field at point
  - `C-c <spc>`
- move to next row and realign table
  - `<ret>`
- move to begining of table field
  - `M-a`
- move to end of table field
  - `M-e`
- [more keybindings](https://orgmode.org/manual/Built_002din-Table-Editor.html)

### column width/alignment

- auto align on startup
  - `org-startup-align-all-tables`
  - per-file
  ``` elisp
  #+STARTUP: align
  #+STARTUP: noalign
  ```
- shrink or expand current column
  - `C-c <tab>`
- set column width
  - seems pretty broken
  - `one field anywhere in the column may contain just the string ‘<N>’`
- shrink columns with specified width
  - `C-u C-c <tab>`
- expand all columns
  - `C-u C-u C-c <tab>`
- auto shrink tables
  - `#+STARTUP: shrink`

### column groups

- use non/data row
  1. start first column with `/`
  1. use `<` to denote start of grouping
  1. use `>` to denote end of grouping
  1. use `<>` to denote single column group

### spreadsheets

- [spreadsheets](https://orgmode.org/manual/The-Spreadsheet.html)

### org-plot

- create graphs of org-mode table data
- [org plot](https://orgmode.org/manual/Org-Plot.html)

## todo

org-mode-todo

### basics 

- denote todo (headline + TODO text)
  - `*** TODO`
- rotate todo state
  - `C-c C-t`
  - `S-<left>` `S-<right>`
- show todo tree
  - `C-c / t`
- insert new todo item (below)
  - `S-M-<ret>`
- show global (aggregated) todo list
  - `M-x org-agenda t`
  - `C-c a t`
- add tag
  - i.e. :atag:btag:sometag:
  - `C-c C-c`
- statistics
  - add statistics cookie to headline
    - `[%]` or `[/]`
  - update statistics cookie
    - `C-c C-c` on cookie

### changing state

- change todo state with timestamp and note
  - `C-u C-c C-t`
  - per-file
    - `#+STARTUP: lognotedone`
    - `#+STARTUP: logdone`
- tracking todo state changes
  - `#+todo: todo(t) wait(w@/!) | done(d!) canceled(c!)`
    - `!` = add time stamp
    - `@` = add note and timestamp
    - `/!` = signals to add timestamp when going backwards
- cycle todo sequence/type
  - `C-S-<left>` `C-S-<right>`
- custom states
  ``` elisp
  (setq org-todo-keywords
      '((sequence "TODO" "PROGRESS" "DONE")))
  ```
  - per-file
    - `#+todo: todo progress verify | done cancled`
    - `#+seq_todo: ...`
- custom types of todo items
  ``` elisp
  (setq org-todo-keywords
      '((type "home" "work" "social" "|" "DONE")))
  ```
  - per-file
    - `#+typ_todo: ...`
- set keyboard shortcut for state
  - `...((type "home(h)" "work(w)" "social(s)"))`

### priorities

- three default priorities
  - `A`-`B`-`C`
- default priority
  - `B`
- set priority
  - `*** todo [#b]`
- number values
  - `*** todo [#1]`
- cycle priorities
  - `S-<up>` `S-<down>`
- custom priorities
  - highest lowest default
  - `#+PRIORITIES: A C B`
  - `#+PRIORITIES: 1 16 32`

### checkboxes

- place checkbox (only in plain list)
  - `[ ]`
- toggle checkbox
  - `C-c C-c`

## tags

tags in org-mode

### tag inheritance

- enrties tags propogate down the list
- set filetags for all all entries
  - `#+FILETAGS: :media:code:offtopic:`

### setting tags

- can add after entry
  - `:tag1:tag2:tag3:`
- enter new tag for headline
  - `C-c C-q`
- enter new tag for entry
  - `C-c C-c`
- adding tag entries
  - `#+tags: @home(w)  @park(h)  @library(b)`
  - `#+tags: laptop(l)  desltop(d)`
- mutually exclusive tags
  - grouped with braces
  - `#+tags: { @home(w)  @park(h)  @library(b) }`

### tag hierarchies

- group tags using
  - `#+tags: [ group1 : tag1 tag2 ]`
  - `#+tags: [ tag1 : tag3 tag4 tag5 ]`
  - `#+tags: [ tag2 : tag6 tag7 ]`
- denote mutually exclusive groups with braces
  - `#+tags: { group1 : tag1 tag2 }`
- regular expressions tags
  ``` elisp
  #+tags: [ music : {m@.+} ]
  #+tags: [ tips : {t@.+} ]
  #+tags: [ games : {g@.+} ]
  #+tags: [ projects : {p@.+} ]
  ```

### tag searches

- sparse tree with all headlines matching tag search
  - `C-c / m`
- global list of tag matches from all agendas
  - `M-x org-agenda m`
  - `M-x org-agenda M`
    - only todo items
    - force checking sub-items

## dates and times

### timestamps

- add timestamp
  - `<2022-10-31 Wed 00:00>`
  - `<2022-10-31 Wed 17:00-22:00>`
- timestamp with repeater interval
  - `<2022-10-30 Wed 00:00 +1w>`
    - repeat every week
  - intervals
    - (d)ays (w)eeks (m)onths (y)
- diary style expressions
  ``` elisp
  * 22:00-23:00 gaming session on every 2nd thursday of the month
  <%%(diary-float t 4 2)>
  ```
  - confused about this
  - check emacs `calendar-date-style` variable for notation
- time/date range
  - `<2022-03-03 Mon>--<2022-10-31 Mon>`
- inactive timestamp
  - insert
    - `C-c !`
  - don't show up in searches
  - wrapped with brackets
  - `[...]`

### creating timestamps

- prompt for date, insert timestamp
  - `C-c .`
- insert inactive timestamp
  - `C-c !`
- normalize, fix timestamp
  - `C-c C-c`
- insert date from calendar
  1. open calendar, move point to date
    - `C-c >`
  1. insert date from calendar
    - `C-c <`
- open agenda for given date
  - `C-c C-o`
- shift date +- 1 day
  - `S-<left>` `S-<right>`
- shift date/time/year/month underneath point
  - `S-<up>` `S-<down>`
- compute time different between range
  - `C-c C-y`
- repeating tasks
  - add to timestamp
    - every day
      - `+1d`
    - every other day
      - `+2d`
    - every week
      - `+2w`

### deadlines and scheduling

- deadline
  - denoting finish by date
  - add deadline
    - `C-c C-d`
    ``` elisp
    ** heading
    DEADLINE: <2022-10-31 Mon 17:00>
    ```
  - remove deadline
    - `C-u C-c C-d`
- scheduled
  - denoting start by date
  - add schedule
    - `C-c C-s`
    ``` elisp
    ** heading
    SCHEDULED: <2022-06-13 17:00>
    ```
  - remove schedule
    `C-u C-c C-s`

## refiling and archiving

### refiling

- refile entry or region at point
  - refile move subtree to new heading
    - `C-c C-w`
  - refile to another file
    1. cut subtree
       - `C-c C-x C-w`
    1. paste subtree in other org file
       - `C-c C-x C-y`
       - if using org roam, can open nodes first
         - `C-c n f`
    1. refile to heading
       - `C-c C-w`
    - see [document editing](#document-editing)

- jump to heading using refile interface
  - `C-u C-c C-w`
- refile copy
  - refile without deleting original
  - `C-c S-w`

### archiving

- archive current entry
  - `C-c C-x C-a`
  - uses value returned by `org-archive-default-command`
- archive subtree from point
  - `C-c C-x C-s`
  - `C-c $`
- per-file archive location
  - `#+ARCHIVE: %s_done::`
- change what meta data is kept in the archive
  - `org-archive-save-context-info`
- internal archiving
  - mark entries archived
    - no moving or copying
    - add `ARCHIVE` tag
    - toggle archive tag
      - `C-c C-x a`
  - check if subtree nodes need archiving
    - `C-u C-c C-x a`
  - cycle tree even if tagged with `ARCHIVE`
    - `C-c C-<tab>`
  - archive in place
    - `C-c C-x A`

## capture and attachments

### capture

- set default notes file
  `org-default-notes-file`
- display capture template menu
  - `M-x org-capture`
  - `C-c c`
- finalize capture
  - `C-c C-c`
- finalize by refiling to different location
  - `C-c C-w`
- abort capture
  - `C-c C-k`
- customize capture templates
  - edit variable
    - `org-capture-templates`
    - set templates
    ``` elisp
    (setq org-capture-templates
      '(("t" "Todo" entry (file+headline "~/org/gtd.org" "Tasks")
         "* TODO %?\n  %i\n  %a")
        ("j" "Journal" entry (file+datetree "~/org/journal.org")
         "* %?\nEntered on %U\n  %i\n  %a")))
    ```
    - set key bind to capture to template
    ``` elisp
    (define-key global-map (kbd "C-c x")
    (lambda () (interactive) (org-capture nil "x")))
    ```
    - configuring templates
      - [template elements](https://orgmode.org/manual/Template-elements.html)
      - [template expansion](https://orgmode.org/manual/Template-expansion.html)
      - [templates in contexts](https://orgmode.org/manual/Templates-in-contexts.html)

### attachments

- start attachments dispatcher
  - `C-c C-a`
- [org attachments options](https://orgmode.org/manual/Attachment-options.html)
- automate attachments versioning with git
  - `(require 'org-attach-git)`

### rss feeds

- add feeds to rss
``` elisp
(setq org-feed-alist
      '(("Slashdot"
         "https://rss.slashdot.org/Slashdot/slashdot"
         "~/txt/org/feeds.org" "Slashdot Entries")))
```
- update feeds
  `C-c C-x g`
- go to inbox of specific feed
  `C-c C-x G`

## agenda

### basics

- get overview of
  - todo items
  - timestamped items
  - tagged headlines
- view types
  - calendar style
  - todo list
  - match view
    - search based on tags, properties, todo state
  - text search view
  - custom views
    - combinations of above
- dispalyed in `agenda buffer`
  - window configuration
    - `org-agenda-restore-windows-after-quit`
    - `org-agenda-window-setup `

### agenda files

- configure directories/files
  - `org-agenda-files`
- add current file to agenda files list
  - `C-c [`
- remove current file from agenda files list
  - `C-c ]`
- cycle agenda files list
  - `C-'` `C-,`
- use minibuffer to switch between org files
  - `M-x org-switchb`
- restrict agenda to current subtree
  - `C-c C-x <`
- remove agenda restriction
  - `C-c C-x >`
- restrict agenda to the item, org file or subtree
  - `<`
- remove restriction
  - `>`

### agenda dispatcher

- start dispatcher
  - `M-x org-agenda`
  - `C-c a`

### agenda commands

- point on agenda item row
  - show org file in another window
    - `<spc>` `L`
  - open org file in another window and delete other windows
    - `<ret>`
  - open org file in another window and switch to it
    - `<tab>`
  - show calendar at date of where point is
    - `c`
- follow mode: always show corresponding agenda file
  - `F`
- interactively append more displays to the current agenda view
  - `A`
- delete other windows
  - `o`
- day/week/month/year view
  - `v d` or `d`
  - `v w` or `w`
  - `v m`
  - `v y`
  - reset view
    `v <spc>`
- moving pointer up/down lines
  - `p` `n`
- moving time forward/backward
  - depends on view mode day/week/month/year
  - `f` `C-u f`
  - `b` `C-u b`
- prompt for date and go
  - `j`
- change todo state
  - `C-c C-t`
- bulk editing
  - mark agenda items
    - `m`
  - unmark agenda items
    - `u`
  - toggle agenda item mark
    - `M-m`
  - mark all visible agenda items
    - `*`
  - unmake all visible agenda items
    - `U`
  - toggle mark for all visible items
    - `M-*`
  - start bulk edit
    - `B`
    - quit
      - `C-g`
- refresh agenda buffer
  - `r`
- quit: remove agenda buffer
  - `q`
- exit: remove agenda buffer and all buffers loaded for compilation of the agenda
  - `x`

### presentation and sorting

- categories
  - dervied from file names
  - can be set using
    - `#+category: music`

### time specifications

- agenda uses regular org timestamps and date ranges
- time grid
  - `org-agenda-use-time-grid`
  - `org-agenda-time-grid`

## org-roam

- what is org-roam
  - creates topic-focused org-files which are linked as a networks of nodes
    - different organization than hierarchical docuemnt structure
  - personal wiki
  - inspired by roam app and zettelkasten note-taking strategy

### nodes

- opening a node
  - `C-c n f`
- creating a node
  - `C-c n f`
  - disable ivy completion
    - `C-M-j`
- link node
  - `C-c n i`
- org-roam-capture
  - `C-c n c`
- org-roam-dailies-capture-today
  - `C-c n j`
- graph
  - `C-c n g`
- deleting nodes
  - delete file
  - `M-x org-roam-db-sync`
- heading node
  - assign org id to header
    - `M-x org-id-get-create`
- alias nodes
  - `M-x org-roam-alias-add`
- viewing backlinks
  - when viewing an org-roam-node
    `C-c n l`
