;;; debug-buffer.el --- provide a debug buffer -*- lexical binding: t; -*-
;;
;; copyright (c) 2022 commet-alt-w
;;
;; author: commet-alt-w <commet-alt-w@protonmail.com>
;; created: 2022 march 09
;; version: 0.1
;; keywords: debug, buffer
;; license: gpl v 3.0
;;
;;; commentary:
;;
;; provide a debug buffer to print to
;;

;;; code:

(require 'debug)

(defcustom debug-buffer-name "*debug*"
  "name of debug-buffer"
  :type 'string
  :require 'debug-buffer
  :group 'debuuger-mode)

;;;###autoload
(defvar debug-buffer nil
  "buffer to send debug data to")

(defun create-debug-buffer()
    (let ((buffer (get-buffer-create debug-buffer-name)))
      (setq debug-buffer buffer)
      (with-current-buffer debug-buffer
        (debugger-mode))))

(defun debug-buffer-exists ()
  "check if debug buffer exists"
  (and debug-buffer
       (member debug-buffer (buffer-list))))

(defun check-debug-create ()
  "create debug buffer if it does not exit"
  (when (not (debug-buffer-exists))
    (create-debug-buffer)))

;;;###autoload
(defun switch-to-debug-buffer-reset ()
  "switch to debug-buffer and reset"
  (interactive)
  (check-debug-create)
  (switch-to-buffer debug-buffer)
  (erase-buffer)
  (insert debug-buffer-name "\n"))

;;;###autoload
(defun print-debug (object &optional add-new-line)
  "print object to debug buffer"
  (check-debug-create)
  (with-current-buffer debug-buffer
    (goto-char (point-max))
    (read-only-mode -1)
    (if (not add-new-line)
        (princ object debug-buffer)
      (print object debug-buffer))
    (read-only-mode 1)))

(provide 'debug-buffer)
(provide 'print-debug)
;;; debug-buffer.el ends here
