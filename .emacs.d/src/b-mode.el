;;; b-mode.el --- vi-like utlity (minor) mode  -*- lexical-binding: t; -*-
;;
;; copyright (c) 2022 commet-alt-w
;;
;; author: commet-alt-w <commet-alt-w@protonmail.com>
;; created: 2021 may 15
;; version: 0.5
;; keywords: utility, vi
;; license: gpl v 3.0
;;
;;; commentary:
;;
;; vi-like utility (minor) mode
;;   b-mode-global is enabled for all system files in any buffer
;;   but not for excluded minor/major modes and, autoloads files,
;;   and user excluded files
;;
;;   please review key-bindings at the end of this file before using
;;   feel free to comment out or delete or change anything
;;
;; keyboard shortcuts:
;;
;;   [toggle on/off]
;;   `i` or `I`
;;       - disables the minor mode
;;       - can use `<insert>` as well
;;   `(b-mode-set-key-binding ...)`
;;       - set global key binding to toggle b-mode on and off per buffer
;;   '(b-mode-global-set-key-binding`
;;       - set global key binding to toggle b-mode on and off for all enabled buffers
;;
;;   [point and edit]
;;   - automatically sets set-goal-column
;;   - mostly remapping emacs functionality,
;;     with some changes, for use with b-mode
;;
;;   `hjkl` or arrow keys
;;       - move left down up right respectively
;;   `HJKL` or `S-<arrow key>`
;;       - highlihght left down up right respectively
;;   `d <left>/<right` or `d h` `d l`
;;       - delete characters left/right
;;   `C-w`
;;       - cut region
;;   `C-k`
;;       - cut line
;;   `C-y`
;;       - paste text
;;   `C-r`
;;       - comment region
;;   `C-x u`
;;       - undo
;;   `C-M-\`
;;       - indent region
;;
;;   [moving/resizing/selecting windows/buffers]
;;   `C-M-w <arrow>`
;;       - move window in given direction
;;   `M-W <arrow>`
;;       - resize window in given direction
;;   `C-M-s <arrow>`
;;       - swap window states with window in the given direction
;;   `C-s-<arrow>`
;;       - select window in given direction
;;
;; usage:
;;     `M-x package-install-file<RET> ../path/to/b-mode.el`
;;
;;     (require 'b-mode)
;;     (b-mode-add-exclude-major-modes 'auto-complete-mode 'sly-mrepl-mode)
;;     (b-mode-set-window-resize-delta 5)
;;     (b-mode-set-key-binding "C-S-b")
;;     (b-mode-set-exclude-autoloads t)
;;     (b-mode-global--set-key-binding "C-C C-b")
;;     (b-mode-add-excluded-files '("file" "file2" ...))

;;; code:

;;;; b-mode vars

(defvar b-mode--key-map (make-sparse-keymap)
  "b-mode key map")
(defvar b-mode--current-column (current-column)
  "last point column stored when starting b-mode or
using h/l keys to move left/right")
(defvar b-mode--window-resize-delta 2
  "delta when resizing windows horizontally or vertically")
(defvar b-mode--highlighting nil
  "flag for when highlighting with <shift> hjkl is enabled")
(defvar b-mode--directions
  '((left . left) (down . below) (up . above) (right . right))
  "custom directions mapped to elisp window directions")
(defvar b-mode--excluded-minor-modes
  '(auto-fill-function)
  "minor modes to exclude from b-mode")
(defvar b-mode--excluded-major-modes
  '(fundamental-mode lisp-interaction-mode minibuffer-inactive-mode
    messages-buffer-mode help-mode debugger-mode)
  "major modes to exclude from b-mode")
(defvar b-mode--excluded-user-files nil
  "files the user explicitly excludes")
(defvar b-mode--exclude-autoloads nil
  "flag to exclude autoloads files from b-mode")

;; buffer local vars
(make-variable-buffer-local 'b-mode--current-column)

;;;; user facing config options

;;;###autoload
(defun b-mode-add-exclude-minor-modes (&rest modes)
  "user function to add minor modes to exclude from b-mode"
  (setq b-mode--excluded-minor-modes (append b-mode--excluded-minor-modes modes)))

;;;###autoload
(defun b-mode-add-exclude-major-modes (&rest modes)
  "user function to add major modes to exclude from b-mode"
  (setq b-mode--excluded-major-modes (append b-mode--excluded-major-modes modes)))

;;;###autoload
(defun b-mode-set-window-resize-delta (delta)
  "user function to set window resize delta"
  (setq b-mode--window-resize-delta delta))

;;;###autoload
(defun b-mode-set-key-binding (binding)
  "set global key binding for toggling b-mode
binding is a string passed to (kbd ...) internally"
  (global-unset-key (kbd binding))
  (define-key global-map (kbd binding) 'b-mode-user-toggle))

;;;###autoload
(defun b-mode-global-set-key-binding (binding)
  "set global key binding for toggling b-mode-global
binding is a string passed to (kbd ...) internally"
  (global-unset-key (kbd binding))
  (define-key global-map (kbd binding) 'b-mode-global-user-toggle))

;;;###autoload
(defun b-mode-set-exclude-autoloads (flag)
  "set non-nil flag to exclude autolaods files from b-mode"
  (setq b-mode--exclude-autoloads flag))

;;;###autoload
(defun b-mode-add-excluded-files (files)
  "add files to append to user excluded files list"
  (setq b-mode--excluded-user-files
        (append b-mode--excluded-user-files files)))

;;;; user facing enabled check

;;;###autoload
(defun b-mode-is-enabled ()
  "return t if b-mode or b-mode-global is enabled"
  (or (b-mode--is-enabled) (b-mode-global--is-enabled)))

;;;; non-user facing enabled check

(defun b-mode--is-enabled ()
  "return b-mode--enabled"
   (and (symbolp 'b-mode) (symbol-value 'b-mode)))

(defun b-mode-global--is-enabled ()
  "return b-mode--enabled"
   (and (symbolp 'b-mode-global) (symbol-value 'b-mode-global)))

;;;; toggle b-mode

(defun b-mode--on ()
  "turn b-mode on"
  (b-mode 1)
  (b-mode--set-current-column)
  (b-mode--set-goal-column)
  (read-only-mode 1))

(defun b-mode--off ()
  "turn b-mode off"
  (b-mode -1)
  (read-only-mode -1))

(defun b-mode-global--on ()
  "turn b-mode-global on"
  (when (b-mode--can-enable (buffer-name))
    (b-mode--on)))

(defun b-mode-global--off ()
  "turn b-mode-global off"
  (dolist (buffer (buffer-list))
    (with-current-buffer buffer
      (when (b-mode--can-enable (buffer-name))
        (b-mode--off)))))

(defun b-mode--has-excluded-minor-mode ()
  "return t if the current buffer has an excluded minor mode enabled"
  (catch 'found
    (dolist (mode b-mode--excluded-minor-modes)
      (when (and (symbolp mode) (symbol-value mode))
        (throw 'found t)))))

(defun b-mode--has-excluded-major-mode ()
  "return t if the current buffer as an excluded major mode enabled"
  (when (member major-mode b-mode--excluded-major-modes)
    t))

(defun b-mode--is-autoloads-file (buffername)
  "return t if buffername is an autolaods file"
  (let* ((target "-autoloads.el")
         (split (split-string buffername target))
         (len (length split)))
    (if (> len 1)
        t
      nil)))

(defun b-mode--is-excluded-user-file ()
  "return t if the buffer is an excluded user file"
  (catch 'found
    (dolist (file b-mode--excluded-user-files)
      (let* ((split (split-string (buffer-file-name) file))
             (len (length split)))
        (when (> len 1)
          (throw 'found t))))))

(defun b-mode--can-enable (buffername)
  "return t if b-mode can be enabled on the buffer"
  (when (and buffername
             (file-exists-p buffername)
             (not
              (and b-mode--exclude-autoloads
                   (b-mode--is-autoloads-file buffername)))
             (not
              (and b-mode--excluded-user-files
                   (b-mode--is-excluded-user-file)))
             (not (b-mode--has-excluded-minor-mode))
             (not (b-mode--has-excluded-major-mode)))
    t))

(defun b-mode--toggle ()
  "toggle b-mode on and off for buffers containing system files"
  (if (b-mode--is-enabled)
      (b-mode--off)
    (when (b-mode--can-enable (buffer-name))
      (b-mode--on))))

(defun b-mode-global-user-toggle ()
  "user action toggle for b-mode-global"
  (interactive)
  (if (b-mode-global--is-enabled)
      (b-mode-global -1)
    (b-mode-global 1)))

;;;###autoload
(defun b-mode-user-toggle ()
  "user action toggle"
  (interactive)
  (b-mode--toggle))

;;;; goal column

;; b-mode point navigation
(defun b-mode--set-goal-column ()
  "set goal-column"
  (setq goal-column b-mode--current-column))

(defun b-mode--set-current-column ()
  "set b-mode--current-column"
  (setq b-mode--current-column (current-column)))

;;;; directions

(defun b-mode--get-opposite-direction (direction)
  "return the opposite direction, where direction is one of
'left 'down 'up 'right"
  (cond ((equal direction 'left)
         'right)
        ((equal direction 'down)
         'up)
        ((equal direction 'up)
         'down)
        ((equal direction 'right)
         'left)))

;;;; moving windows

;; edge checks
;;   emacs provides functionality for enlarging and shrinking windows
;;   this inverts some intuitive ux where up/down and left/right are inverted in
;;   some circumstances. these edge check funcallables help with correcting
;;   those inversions to expected behavior. they make use of checking against
;;   window-edges of the current window, or first-window and current window edges.
;;   window-edges are (left top right bottom)
(defun b-mode--window-left-edge-check ()
  "return true if windows left edge is 0"
  (equal 0 (car (window-edges))))

;; notes:
;;  will the frames first window top edge always be 0?
;;  the issue here logically might just be that there needs to be a shortcuit in the
;;  subroutine, so maybe there's a better way to handle that
(defun b-mode--window-edges-check ()
  "check window edges
returns true if left edge is 0, or if the bottom edge of the frames
first window and the current window are not equal and the frames first window
top edge is 0"
  ;; check if left edge is 0
  (or (b-mode--window-left-edge-check)
      (and
       ;; check if bottom edge of frames first window
       ;; and current-window are not equal
       (not (equal (nth 3 (window-edges)) (nth 3 (cadr (car (window-tree))))))
       ;; check if frames first window top edge is 0
       (equal 0 (cadr (cadr (car (window-tree))))))))

;; moving buffers to new windows
(defun b-mode--window-in-direction (direction)
  "return the window in a given direction where direction
is one of 'left 'down 'up 'right"
  (window-in-direction (cdr (assq direction b-mode--directions))))

;; splitting windows
(defun b-mode--split-window (&optional window size side pixelwise)
  "return window after splitting in a given direction
where direction is one of 'left 'down 'up 'right"
  (split-window window size (cdr (assq side b-mode--directions)) pixelwise))

(defun b-mode--window-can-expand ()
  "return true if there's windows stacked vertically,
but no windows on the left or right"
  (and (not (b-mode--window-in-direction 'left))
       (not (b-mode--window-in-direction 'right))
       (or (b-mode--window-in-direction 'up)
           (b-mode--window-in-direction 'down))))

(defun b-mode--new-window-swap-buffers (current-window target-window direction)
  "create new window in direction, swap buffers, and delete old window"
  ;; get new window
  (let ((new-window
         (if target-window
             ;; target window exists towards intended direction
             (progn (select-window target-window)
                    ;; create new window by splitting
                    (split-window (get-buffer-window) nil))
           ;; no target window
           (progn
             ;; create new window by splitting parent window
             (b-mode--split-window (window-parent) nil direction)))))
    ;; swap window states and delete old window
    (window-swap-states current-window new-window)
    (select-window new-window)
    (delete-window current-window)))

(defun b-mode--num-windows-in-direction (direction)
  "return the number of windows in a given direction
where direction is one of 'left 'down 'up 'right"
  (let ((count 0)
        (current-window (get-buffer-window))
        (target-window (b-mode--window-in-direction direction)))
    (while target-window
      (setq count (1+ count))
      (select-window target-window)
      (setq target-window (b-mode--window-in-direction direction)))
    (select-window current-window)
    count))

(defun b-mode--move-window-horizontally (current-window target-window direction)
  "move window horizontally"
  ;; create new window and swap buffers
  (b-mode--new-window-swap-buffers
   current-window target-window direction)
  ;; resize other windows evenly
  (balance-windows))

(defun b-mode--move-window-vertically (target-window direction)
  "move window vertically"
  ;; get number of windows in intended direction and its opposite direction
  (let* ((opposite-direction
          (b-mode--get-opposite-direction direction))
         (num-windows
          (b-mode--num-windows-in-direction direction))
         (num-windows-opposite
          (b-mode--num-windows-in-direction opposite-direction)))
    ;; allow window to expand vertically if window is on a
    ;; vertical column edge and has more than 1 other window
    ;; in the column
    (when (and (equal 0 num-windows)
               (> num-windows-opposite 1))
      ;; resize window
      ;; resize in opposite direction when direction is up
      (if (equal direction 'up)
          (b-mode--resize-window-direction opposite-direction)
        (b-mode--resize-window-direction direction)))
    ;; no need to expand, only swap states
    ;; if there's a window in the intended direction
    (when target-window
      (b-mode--swap-window-states direction)
      (balance-windows))))

;; todo: top left goes to bottom right, gotta fix that
;;  top left should go to top right
(defun b-mode--move-window-in-direction (direction)
  "move window in a given direction
where direction is one of 'left 'down 'up 'right. 
either only swapping buffers, or creating new windows, then swapping those buffers
and deleting the old window. there are no edge most check to prevent users from 
trying to move the window, so this will expand the edge most window when there are 
more windows stacked vertically or horiontall respective of intended direction."
  (let* ((current-window (get-buffer-window))
         (target-window
          (b-mode--window-in-direction direction)))
    (cond
     ;; movement is left or right
     ((or (equal direction 'left)
          (equal direction 'right))
      (b-mode--move-window-horizontally
       current-window target-window direction))
     ;; movement is down or up
     ((or (equal direction 'down)
          (equal direction 'up))
      (b-mode--move-window-vertically target-window direction)))))

(defun b-mode--is-window-any-direction ()
  "check if there are other windows in any direction"
  (or (b-mode--window-in-direction 'left)
      (b-mode--window-in-direction 'down)
      (b-mode--window-in-direction 'up)
      (b-mode--window-in-direction 'right)))

(defun b-mode--can-move-window (direction)
  "check if a window can be moved"
  (cond ((equal direction 'left)
         (b-mode--is-window-any-direction))
        ((equal direction 'down)
         (b-mode--is-window-any-direction))
        ((equal direction 'up)
         (b-mode--is-window-any-direction))
        ((equal direction 'right)
         (b-mode--is-window-any-direction))))

(defun b-mode-move-window-left ()
  "move window to the left"
  (interactive)
  (if (b-mode--can-move-window 'left)
      (b-mode--move-window-in-direction 'left)
    (message "unable to move window left")))

(defun b-mode-move-window-down ()
  "move window down"
  (interactive)
  (if (b-mode--can-move-window 'down)
      (b-mode--move-window-in-direction 'down)
    (message "unable to move window down")))

(defun b-mode-move-window-up ()
  "move window up"
  (interactive)
  (if (b-mode--can-move-window 'up)
      (b-mode--move-window-in-direction 'up)
    (message "unable to move window up")))

(defun b-mode-move-window-right ()
  "move window to the right"
  (interactive)
  (if (b-mode--can-move-window 'right)
      (b-mode--move-window-in-direction 'right)
    (message "unable to move window right")))

;;;; window resizing

(defun b-mode--can-resize-window (direction)
  "return t if window can be resized"
  (and
   ;; not a root window
   (window-parent)
   ;; number of windows in intended direction, or
   ;; it's opposite, is more than 0
   (let ((opposite-direction
          (b-mode--get-opposite-direction direction)))
     (or (> (b-mode--num-windows-in-direction direction) 0)
         (> (b-mode--num-windows-in-direction opposite-direction) 0)))))

(defun b-mode--resize-window-direction (direction)
  "resize a window in the intended direction"
  (if (b-mode--can-resize-window direction)
    (cond ((equal direction 'left)
           (if (b-mode--window-left-edge-check)
               (window-resize nil (- b-mode--window-resize-delta) t)
             (window-resize nil b-mode--window-resize-delta t)))
          ((equal direction 'down)
           (if (b-mode--window-edges-check)
               (window-resize nil b-mode--window-resize-delta)
             (window-resize nil (- b-mode--window-resize-delta))))
          ((equal direction 'up)
           (if (b-mode--window-edges-check)
               (window-resize nil (- b-mode--window-resize-delta))
             (window-resize nil b-mode--window-resize-delta)))
          ((equal direction 'right)
           (if (b-mode--window-left-edge-check)
               (window-resize nil b-mode--window-resize-delta t)
             (window-resize nil (- b-mode--window-resize-delta) t))))
    (message (format "unable to resize window %s" direction))))

(defun b-mode-resize-window-left ()
  "resize window moving left"
  (interactive)
  (b-mode--resize-window-direction 'left))

(defun b-mode-resize-window-down ()
  "resize window moving down"
  (interactive)
  (b-mode--resize-window-direction 'down))

(defun b-mode-resize-window-up ()
  "resize window moving up"
  (interactive)
  (b-mode--resize-window-direction 'up))

(defun b-mode-resize-window-right ()
  "resize window moving right"
  (interactive)
  (b-mode--resize-window-direction 'right))

;;;; swapping window states

(defun b-mode--swap-window-states (direction)
  "swap selected window state with the window of state of a window in a direction
where direction is one of 'left 'down 'up 'right"
       (let ((target-window (b-mode--window-in-direction direction)))
         (window-swap-states (selected-window) target-window)))

(defun b-mode-swap-window-states-left ()
  "swap window states moving to the left"
  (interactive)
  (if (b-mode--window-in-direction 'left)
      (b-mode--swap-window-states 'left)
    (message "unable to move window left")))

(defun b-mode-swap-window-states-down ()
  "swap window states moving down"
  (interactive)
  (if (b-mode--window-in-direction 'down)
      (b-mode--swap-window-states 'down)
    (message "unable to move window down")))

(defun b-mode-swap-window-states-up ()
  "swap windows states moving up"
  (interactive)
  (if (b-mode--window-in-direction 'up)
      (b-mode--swap-window-states 'up)
    (message "unable to move window up")))

(defun b-mode-swap-window-states-right ()
  "swap window states moving to the right"
  (interactive)
  (if (b-mode--window-in-direction 'right)
      (b-mode--swap-window-states 'right)
    (message "unable to move window right")))

;;;; selecting windows

(defun b-mode--select-window (direction)
  "select the window in the intended direction"
  (let ((target-window (b-mode--window-in-direction direction)))
    (when target-window
        (select-window target-window))))

(defun b-mode-select-window-left ()
  "select window left of current window"
  (interactive)
  (b-mode--select-window 'left))

(defun b-mode-select-window-down ()
  "select window down of current window"
  (interactive)
  (b-mode--select-window 'down))

(defun b-mode-select-window-up ()
  "select window up of current window"
  (interactive)
  (b-mode--select-window 'up))

(defun b-mode-select-window-right ()
  "select window right of current window"
  (interactive)
  (b-mode--select-window 'right))

;;;; point, marker, and remapping emacs functionality

;; gets annoying to keep toggling read only when writing some remapping functions
;; can pass a lambda expression into this function to do it for you
(defun b-mode--read-only-pass (l)
  "disable read-only-mode, run the lambda expression, and re-enable read-only-mode"
  (read-only-mode -1)
  (funcall l)
  (read-only-mode 1))

(defun b-mode--deactivate-mark ()
  "b-mode wrapper to deactivate mark if a region is active when hjkl are pressed"
  (when (and
         (b-mode-is-enabled) b-mode--highlighting (region-active-p))
    (deactivate-mark)))

(defun b-mode--move-point (direction)
    "move point in direction
where direction is one of left, down, up, right"
    (cond ((equal direction 'left)
           (b-mode--set-current-column)
           (left-char))
          ((equal direction 'down)
           (b-mode--set-goal-column)
           (line-move 1 nil nil t))
          ((equal direction 'up)
           (b-mode--set-goal-column)
           (line-move -1 nil nil t))
          ((equal direction 'right)
           (b-mode--set-current-column)
           (right-char)))
    (b-mode--set-goal-column))

(defun b-mode--set-marker-move (direction)
  "set mark and move in a given direction
where direction is one of left, down, up, right"
  (setq b-mode--highlighting t)
  (b-mode--set-current-column)
  (b-mode--set-goal-column)
  (set-mark (point))
  (b-mode--move-point direction))

;;;; b-mode key map functions

;; hjkl movement functions

(defun b-mode-left-one-char ()
  "move point left one character and store column"
  (interactive)
  (when (region-active-p)
    (b-mode--deactivate-mark))
  (b-mode--move-point 'left))

(defun b-mode-down-one-line ()
  "move point down one line"
  (interactive)
  (when (region-active-p)
    (b-mode--deactivate-mark))
  (b-mode--move-point 'down))

(defun b-mode-up-one-line ()
  "move point up one line"
  (interactive)
  (when (region-active-p)
    (b-mode--deactivate-mark))
  (b-mode--move-point 'up))

(defun b-mode-right-one-char ()
  "move point right one character and store column"
  (interactive)
  (when (region-active-p)
    (b-mode--deactivate-mark))
  (b-mode--move-point 'right))

;; commenting and uncommenting an active region
(defun b-mode-comment-region ()
  "comment or uncomment an active region"
  (interactive)
  (when (region-active-p)
    (let ((start (line-number-at-pos (region-beginning)))
          (end (line-number-at-pos (region-end))))
      (b-mode--read-only-pass
       (lambda ()
         (comment-line (- end start)))))))

;; indenting an active region
(defun b-mode-indent-region ()
  "indent an active region"
  (interactive)
  (when (region-active-p)
    (let ((start (line-number-at-pos (region-beginning)))
          (end (line-number-at-pos (region-end))))
      (b-mode--read-only-pass
       (lambda ()
         (indent-region start end))))))

(defun b-mode-move-end-of-line ()
  "move end of line with set goal-column"
  (interactive)
  (move-end-of-line 1)
  (b-mode--set-current-column)
  (b-mode--set-goal-column))

;; highlighting regions with hjkl
(defun b-mode-highlight-left ()
  "highlight left one char"
  (interactive)
  (if (not b-mode--highlighting)
      (b-mode--set-marker-move 'left)
    (b-mode--move-point 'left)))

(defun b-mode-highlight-down ()
  "highlight down one line"
  (interactive)
  (if (not b-mode--highlighting)
      (b-mode--set-marker-move 'down)
    (b-mode--move-point 'down)))

(defun b-mode-highlight-up ()
  "highlight up one line"
  (interactive)
  (if (not b-mode--highlighting)
      (b-mode--set-marker-move 'up)
    (b-mode--move-point 'up)))

(defun b-mode-highlight-right ()
  "highlight right one char"
  (interactive)
  (if (not b-mode--highlighting)
      (b-mode--set-marker-move 'right)
    (b-mode--move-point 'right)))

;; deleteing chars left and right
(defun b-mode-delete-char-left ()
  "delete one character to the left of the point"
  (interactive)
  (b-mode--read-only-pass
   (lambda ()
     (delete-char -1))))

(defun b-mode-delete-char-right ()
  "delete one character to the left of the point"
  (interactive)
  (b-mode--read-only-pass
   (lambda ()
     (delete-char 1))))

;; remaping normal emacs functions to b-mode
(defun b-mode-undo ()
  "undo"
  (interactive)
  (b-mode--read-only-pass
   (lambda ()
     (undo))))

(defun b-mode-cut ()
  "cut"
  (interactive)
  (b-mode--read-only-pass
   (lambda ()
     (kill-region (region-beginning) (region-end)))))

(defun b-mode-paste ()
  "paste"
  (interactive)
  (b-mode--read-only-pass
   (lambda ()
     (yank))))

(defun b-mode-cut-line ()
  "cute line"
  (interactive)
  (b-mode--read-only-pass
   (lambda ()
     (kill-line))))

;;;; b-mode hooks

;; find-file-hooks
(defun b-mode--find-file-hook ()
  "b-mode function for find-file-hooks"
  (when (and (b-mode-is-enabled)
             (b-mode--can-enable (buffer-name)))
    (b-mode--on)))

;; after-load-functions
(defun b-mode--after-load-hook (file-name)
  "b-mode function for enabling b-mode when b-mode.el is reloaded"
  (when
      (equal "b-mode.el" (file-relative-name file-name))
    (b-mode--on)))

;; deactivate-mark-hook
(defun b-mode--deactivate-mark-hook ()
  "b-mode function to disable highlighting flag when mark is deactivated"
  (when (and (b-mode-is-enabled) b-mode--highlighting (not (region-active-p)))
    (setq b-mode--highlighting nil)))

;;;; b-mode

;;;###autoload
(define-minor-mode b-mode
  "vi-like utility (minor) mode"
  :init-value nil
  :global nil
  :lighter " b-mode"
  :keymap b-mode--key-map
  (if b-mode
      (message "b-mode activated")
    (message "b-mode deactivated")))

;;;###autoload
(define-globalized-minor-mode b-mode-global b-mode b-mode-global--on
  (if b-mode-global
      (message "b-mode-global activated")
    (progn (message "b-mode-global deactivated")
           (b-mode-global--off))))

;;;; b-mode key bindings

;; disable b-mode
(define-key b-mode--key-map (kbd "i") 'b-mode-user-toggle)
(define-key b-mode--key-map (kbd "I") 'b-mode-user-toggle)
(define-key b-mode--key-map (kbd "<insert>") 'b-mode-user-toggle)

;; hjkl vi-like bindings
(define-key b-mode--key-map (kbd "h") 'b-mode-left-one-char)
(define-key b-mode--key-map (kbd "j") 'b-mode-down-one-line)
(define-key b-mode--key-map (kbd "k") 'b-mode-up-one-line)
(define-key b-mode--key-map (kbd "l") 'b-mode-right-one-char)

;; remap arrow key
;;(define-key b-mode--key-map (kbd "<left>") 'b-mode-left-one-char)
;;(define-key b-mode--key-map (kbd "<down>") 'b-mode-down-one-line)
;;(define-key b-mode--key-map (kbd "<up>") 'b-mode-up-one-line)
;;(define-key b-mode--key-map (kbd "<right>") 'b-mode-right-one-char)

;; resize window
(define-key b-mode--key-map (kbd "M-W <left>") 'b-mode-resize-window-left)
(define-key b-mode--key-map (kbd "M-W <down>") 'b-mode-resize-window-down)
(define-key b-mode--key-map (kbd "M-W <up>") 'b-mode-resize-window-up)
(define-key b-mode--key-map (kbd "M-W <right>") 'b-mode-resize-window-right)

;; swapping buffers
(define-key b-mode--key-map (kbd "C-M-s <left>") 'b-mode-swap-window-states-left)
(define-key b-mode--key-map (kbd "C-M-s <down>") 'b-mode-swap-window-states-down)
(define-key b-mode--key-map (kbd "C-M-s <up>") 'b-mode-swap-window-states-up)
(define-key b-mode--key-map (kbd "C-M-s <right>") 'b-mode-swap-window-states-right)

;; move window
(define-key b-mode--key-map (kbd "C-M-w <left>") 'b-mode-move-window-left)
(define-key b-mode--key-map (kbd "C-M-w <down>") 'b-mode-move-window-down)
(define-key b-mode--key-map (kbd "C-M-w <up>") 'b-mode-move-window-up)
(define-key b-mode--key-map (kbd "C-M-w <right>") 'b-mode-move-window-right)

;; select window
(define-key b-mode--key-map (kbd "C-s-<left>") 'b-mode-select-window-left)
(define-key b-mode--key-map (kbd "C-s-<down>") 'b-mode-select-window-down)
(define-key b-mode--key-map (kbd "C-s-<up>") 'b-mode-select-window-up)
(define-key b-mode--key-map (kbd "C-s-<right>") 'b-mode-select-window-right)

;; comment region
(define-key b-mode--key-map (kbd "C-r") 'b-mode-comment-region)

;; indent region
(define-key b-mode--key-map (kbd "C-M-\\") 'b-mode-indent-region)

;; move to end of line
(define-key b-mode--key-map (kbd "C-e") 'b-mode-move-end-of-line)

;; highlight region
(define-key b-mode--key-map (kbd "H") 'b-mode-highlight-left)
(define-key b-mode--key-map (kbd "J") 'b-mode-highlight-down)
(define-key b-mode--key-map (kbd "K") 'b-mode-highlight-up)
(define-key b-mode--key-map (kbd "L") 'b-mode-highlight-right)
(define-key b-mode--key-map (kbd "S-<left>") 'b-mode-highlight-left)
(define-key b-mode--key-map (kbd "S-<down>") 'b-mode-highlight-down)
(define-key b-mode--key-map (kbd "S-<up>") 'b-mode-highlight-up)
(define-key b-mode--key-map (kbd "S-<right>") 'b-mode-highlight-right)

;; delete character left/right
(define-key b-mode--key-map (kbd "d <left>") 'b-mode-delete-char-left)
(define-key b-mode--key-map (kbd "d h") 'b-mode-delete-char-left)
(define-key b-mode--key-map (kbd "d <right>") 'b-mode-delete-char-right)
(define-key b-mode--key-map (kbd "d l") 'b-mode-delete-char-right)

;; remap global-map emacs functions to b-mode
(define-key b-mode--key-map (kbd "C-x u") 'b-mode-undo)
(define-key b-mode--key-map (kbd "C-w") 'b-mode-cut)
(define-key b-mode--key-map (kbd "C-y") 'b-mode-paste)
(define-key b-mode--key-map (kbd "C-k") 'b-mode-cut-line)

;;;; hooks

;; add a hook to enable b-mode for system files opened with find-new-file command
(add-hook 'find-file-hook 'b-mode--find-file-hook)
;; add hook to enable b-mode if b-mode.el is ever reloaded
(add-hook 'after-load-functions 'b-mode--after-load-hook)
;; hook to disable highlighting flag when mark is deactivated
(add-hook 'deactivate-mark-hook 'b-mode--deactivate-mark-hook)

(provide 'b-mode)
(provide 'b-mode-global)
;;; b-mode.el ends here
