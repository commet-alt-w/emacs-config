;;; init.el --- emacs config -*- lexical-binding: t; -*-
;;
;; copyright (c) 2022 commet-alt-w
;;
;; author: commet-alt-w <commet-alt-w@protonmail.com>
;; license: gnu gpl v3.0
;;
;;; commentary:
;;
;; https://gitlab.com/commet-alt-w/emacs-config
;;

;;; code

;;;;;;;;;;;;;;;;;;;;;;
;;;; requirements ;;;;
;;;;;;;;;;;;;;;;;;;;;;

;; desktop
(require 'desktop)
;; todo: need a new method to save desktops
;; basically all the same functinality, but just don't revert on start
;;(desktop-save-mode 1)

;; recentf
(require 'recentf)

;; org-mode attachment versioning
(require 'org-attach-git)

;; cl-lib
(require 'cl-lib)

;; compile
(require 'compile)

;; eshell
(require 'shell)
(require 'eshell)
(require 'esh-mode)
(require 'em-cmpl)

;; line numbers
(require 'display-line-numbers)

;; package
(require 'package)

;;;;;;;;;;;;;;;;;;;;;;;
;;;; package repos ;;;;
;;;;;;;;;;;;;;;;;;;;;;;

;; load and activate emacs packages
;;(package-initialize)

;; pckage archives 
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/") t)

(defvar package-list nil
  "list of packages to install")
(setq package-list
      '(ac-capf ac-clang ac-sly ace-window all-the-icons all-the-icons-ivy
        all-the-icons-ivy-rich async auto-complete cmake-ide cmake-mode
        counsel dashboard deft diminish doom-modeline doom-themes
        eshell-git-prompt eshell-toggle expand-region flycheck flymake-shell
        flymake-shellcheck flyspell-correct-ivy glsl-mode ivy ivy-hydra
        latex-preview-pane magit markdown-mode meson-mode mini-frame neotree
        nov nyan-mode org-modern org-roam org-roam-ui org-view-mode pdf-tools
        persistent-scratch pkg-info projectile rtags sly ssh-agency undo-fu
        undo-fu-session undo-tree vala-mode workgroups2))

(defun forward-one-symbol ()
  "move point forward exactly one symbol"
  (forward-symbol 1))

(defun forward-one-whitespace ()
  "move point forward exactly one whitespace"
  (forward-whitespace 1))

(defun sort-package-list ()
  "sort package-list in init.el alphabetically and save init.el"
  (interactive)
  (let ((buffer (get-buffer "init.el")))
    (unless (and buffer (not buffer-read-only))
      (error "init.el must be open in a buffer and not read-only"))
    ;; switch to init.el buffer
    (switch-to-buffer buffer)
    ;; go to start of file
    (goto-char (point-min))
    ;; find start and end of package-list definition
    (let ((start (search-forward-regexp "package-list[\t\n\\s-*]-*"))
          (end (forward-list)))
      ;; isolate package names
      (narrow-to-region (+ start 8) (- end 1))
      (delete-whitespace-rectangle (point-min) (point-max))
      (goto-char (point-min))
      (let ((inhibit-field-text-motion t))
        ;; fit to column
        (fill-region (point-min) (point-max))
        ;; sort
        (sort-subr nil 'forward-one-whitespace 'forward-one-symbol)
        ;; fit to column
        (fill-region (point-min) (point-max)))
      (widen)
      (indent-region start end)
      (save-buffer))))

(defun install-packages ()
  "get package list, install missing packages"
  (interactive)
  (unless package-archive-contents
    (package-refresh-contents))
  ;; install missing packages
  (dolist (pkg package-list)
    (unless (package-installed-p pkg)
      (package-install pkg))))

(defun package-version-= (v1 v2)
  "return t if two emacs package versions are equal"
  (and (= (car v1) (car v2))
       (= (car (cdr v1)) (car (cdr v2)))))

(defun update-packages ()
  "update installed packages"
  (interactive)
  ;; refresh packages list
  (unless package-archive-contents
    (package-refresh-contents))
  (let ((pkgs nil))
    ;; compare local and remote package versions
    (dolist (pkg package-list)
      ;; get package name and version for current/remote
      (let ((pkg-name
             (package-desc-name (cadr (assq pkg package-alist))))
            (current-version
             (package-desc-version (cadr (assq pkg package-alist))))
            (repo-version
             (package-desc-version (cadr (assq pkg package-archive-contents)))))
        ;; add the package to update list if versions are not the same
        (unless (package-version-= current-version repo-version)
          (push (list pkg-name repo-version) pkgs))))
    ;; if we have package to udpate
    (if (> (length pkgs) 0)
        ;; update packages
        (let ((pkg-trans
               (package-compute-transaction () pkgs)))
          (package-download-transaction pkg-trans)))))

(defmacro enable-package (pkg &optional eal &rest body)
  "macro to enable a package for use with emacs. won't try to enable if
it's not installed. can pass any necessary config code here too, set eal
to t if you want to eval the config code after loading the package."
  `(when (package-installed-p ,pkg)
    (require ,pkg)
    (if ,eal
        (with-eval-after-load ,pkg
         ,@body)
      (progn ,@body))))

;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; custom set vars ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;

(let* ((desktop-dir "~/.emacs.d/desktop")
       (desktop-filename "emacs.desktop")
       (bookmarks-dir "~/.emacs.d/bookmarks")
       (org-mode-directory "~/.emacs.d/org-mode")
       (org-mode-archive-dir (concat org-mode-directory "/archive/%s_archive::"))
       (emacs-saves-dir "~/.emacs.d/.emacs-saves/")
       (todo-keywords
        '((sequence "todo(t!)" "progress(p@/!)" "|" "done(d!) canceled(c@)"))))
  ;; custom-set-variables
  (custom-set-variables
   ;; prevent custom from writing to init.el
   `(custom-file "~/.emacs.d/custom.el")
   ;; debugging
   `(debug-on-error nil)
   ;; desktop session saving
   `(desktop-base-file-name ,desktop-filename)
   `(desktop-base-lock-name "lock")
   `(desktop-path '(,desktop-dir))
   `(desktop-dirname ,desktop-dir)
   `(desktop-save t)
   `(desktop-files-not-to-save "^$")
   `(desktop-load-locked-desktop t)
   `(desktop-auto-save-timeout 30)
   ;; utf-8
   `(locale-coding-system 'utf-8)
   ;; recentf
   `(recentf-max-menu-items 64)
   `(recentf-max-saved-items 64)
   `(recentf-auto-cleanup 'never)
   ;; eshell
   ;;`(eshell-prompt-regexp, "[:][)]")
   ;;`(eshell-prompt-function, 'eshell-prompt-b)
   ;; disable native compilation
   ;; todo: these are not solutions for disabling compilation done by
   ;;  emacs being compiled with `--with-native-comp` flag
   `(package-native-compile nil)
   `(no-native-compile t)
   ;; todo: need to test if this will work
   `(comp-deferred-compilation nil)
   ;; backup options
   `(backup-by-copying t)
   `(backup-directory-alist '(("." . ,emacs-saves-dir)))
   `(delete-old-versions t)
   `(kept-new-versions 6)
   `(kept-old-versions 2)
   `(version-control t)
   `(auto-save-file-name-transforms '((".*" ,emacs-saves-dir t)))
   ;; spaces for tabs
   `(indent-tabs-mode nil)
   `(tab-width 2)
   ;; disable bell
   ;;`(ring-bell-function 'ignore)
   ;; header line
   ;; add padding/margin at top in gui-mode
   `(header-line-format "")
   ;; disable startup message
   `(inhibit-startup-message t)
   ;; line number format
   `(linum-format "%4d /u2502 ")
   ;; wrap lines
   `(truncate-lines t)
   ;; bookmarks
   `(bookmark-file (concat ,bookmarks-dir "/bookmarks"))
   ;; calendar
   `(calendar-date-style 'iso)
   ;; org-mode
   `(org-directory ,org-mode-directory)
   `(org-archive-location ,org-mode-archive-dir)
   `(org-log-done 'time)
   `(org-blank-before-new-entry '((heading . t) (plain-list-item . nil)))
   `(org-todo-keywords ',todo-keywords)
   `(org-enforce-todo-dependencies t)
   `(org-startup-indented t)
   `(org-startup-align-all-tables t)
   `(org-catch-invisible-edits t)
   ;;`(org-src-preserve-indentation nil)
   ;;`(org-edit-src-content-indentation 0)
   `(org-support-shift-select t)
   `(org-agenda-restore-windows-after-quit nil)
   `(org-agenda-files '(,org-mode-directory))
   `(org-agenda-include-diary t)
   `(org-default-notes-file (concat ,org-mode-directory "/notes.org"))
   `(org-num-skip-unnumbered t)))

;;;;;;;;;;;;
;;;; ui ;;;;
;;;;;;;;;;;;

;; enable buffer history session saving
(savehist-mode 1)

;; enable commands
(put 'erase-buffer 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'scroll-left 'disabled nil)
(put 'scroll-right 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'set-goal-column 'disabled nil)

;; utf-8
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

;; mouse color
(set-mouse-color "#ffffff")

;; (defvar display-line-numbers-exempt-modes
;;   '(neotree-mode debugger-mode lisp-interaction-mode markdown-mode
;;     messages-buffer-mode info-mode nov-mode tex-mode dashboard-mode)
;;   "list of major modes to exclude from global-display-line-numbers-mode")

;; (defun display-line-numbers--turn-on ()
;;   "excluded modes from line numbers `display-line-numbers-exempt-modes'"
;;   (if (and
;;        (not (member major-mode display-line-numbers-exempt-modes))
;;        (not (minibufferp)))
;;       (display-line-numbers-mode)))

;; enable global line numbers
;(global-display-line-numbers-mode t)

;; disable menu-bar
(menu-bar-mode -1)

;; initialize font
(defun init-font ()
  ;; set font
  ;; not sure this works at all
  ;; set font in .xresources or w.e. you `xrdb -load` for 
  (set-face-attribute 'default t :font "Hack")
  (set-face-attribute 'default nil :height 100)
  (set-frame-font "Hack" nil t))

;; disable bars
(defun disable-bars ()
  ;; disable scroll-bar and tool-bar in gui mode
  (tool-bar-mode -1)
  (scroll-bar-mode -1))

;; init font and disbale bars in graphic mode
(when (display-graphic-p)
  (init-font)
  (disable-bars))

;; global font lock
(global-font-lock-mode 1)

;;;;;;;;;;;;;;;
;;;; lists ;;;;
;;;;;;;;;;;;;;;

;; add .gitignore files to conf-space-mode
(add-to-list 'auto-mode-alist '("/.gitignore\\'" . conf-space-mode))

;; display epub files using nov.el (nov-mode) 
(add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))

;; use shell-script-mode for apkbuild files
(add-to-list 'auto-mode-alist '("\\APKBUILD\\'" . shell-script-mode))

;; have recentf ignore .org files
(add-to-list 'recentf-exclude "\\.org")
(add-to-list 'recentf-exclude "\\-autoloads.el")

;; c dev path
(defvar dev-path nil
  "path of include directories")
(setq dev-path '("/usr/include/libevdev-1.0"))

;;;;;;;;;;;;;;;
;;;; hooks ;;;;
;;;;;;;;;;;;;;;

;; change lighter for emacs-lisp-mode
(add-hook 'emacs-lisp-mode-hook (lambda () (setq mode-name "elisp")))

;; enable show-paren-mode when in emacs lisp major mode
(add-hook 'emacs-lisp-mode-hook (lambda () (show-paren-mode t)))

;; re-load stuff the client frame missed when starting emacsclient
(add-hook 'server-after-make-frame-hook
          (lambda () (init-font) (disable-bars)))

;; unbind c-mode's c-set-style
(add-hook 'c-mode-hook (lambda () (local-unset-key (kbd "C-c ."))))

;; start recentf after emacs startup
(add-hook 'emacs-startup-hook (lambda () (recentf-mode 1)))

;; set tabs/spaces settings for all major modes
;; (add-hook 'after-change-major-mode-hook
;;           (lambda ()
;;             (setq indent-tabs-mode nil)
;;             (setq tab-width 2)))

;; set 256 color term mode for eshell
(add-hook 'eshell-mode-hook
          (lambda ()
            (setenv "TERM" "xterm-256color")))

;;;;;;;;;;;;;;;;
;;;; advice ;;;;
;;;;;;;;;;;;;;;;

(defun custom-set-goal-column (&rest _args)
  "custom funcallable to set goal column"
  (setq goal-column (current-column)))

;; set goal column when moving point with mouse
;; or when using left/right movement
(advice-add 'mouse-set-point :after #'custom-set-goal-column)
(advice-add 'right-char :after #'custom-set-goal-column)
(advice-add 'left-char :after #'custom-set-goal-column)

;;;;;;;;;;;;;;;;;;;;;
;;;; keybindings ;;;;
;;;;;;;;;;;;;;;;;;;;;

;; disable overwite insert key
(global-unset-key (kbd "<insert>"))

;; org-mode
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)

;; line numbers
(global-set-key (kbd "C-S-l") 'display-line-numbers-mode)

;; bind `C-x C-b` (list-buffers) to ivy-switch-buffer
(global-set-key (kbd "C-x C-b") 'ivy-switch-buffer)

;; bind 'M-c` (capitalize-word) to calendar
(global-set-key (kbd "M-c") 'calendar)

;; spawn eshell
(global-set-key (kbd "<C-s-return>") 'eshell)

;; comment/uncomment region
(global-set-key (kbd "C-<") 'uncomment-region)
(global-set-key (kbd "C->") 'comment-region)

;; unbind C-z suspend-frame
(global-unset-key (kbd "C-z"))

;;;;;;;;;;;;;;;;;;;;;;;;
;;;; package config ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;

;; workgroups2
(enable-package 'workgroups2 nil
                (workgroups-mode 1)
                (custom-set-variables
                 `(wg-prefix-key "C-c z")
                 `(wg-session-file "~/.emacs.d/desktop/emacs.workgroups2")))

;; ace-window
(enable-package 'ace-window nil
                (global-set-key (kbd "M-o") 'ace-window))

;; eshell-git-prompt
(enable-package 'eshell-git-prompt nil
                 (eshell-git-prompt-use-theme 'simple))

;; eshell-toggle
(enable-package 'eshell-toggle nil
               (global-set-key (kbd "M-`") 'eshell-toggle))

;; dashboard
(enable-package 'dashboard nil
                (dashboard-setup-startup-hook)
                (let ((dashboard-name "*dashboard*"))
                  (custom-set-variables
                   `(dashboard-center-content t)
                   `(dashboard-set-heading-icons t)
                   `(dashboard-set-file-icons t)
                   `(dashboard-set-navigator t)
                   `(dashboard-buffer-name ,dashboard-name)
                   `(dashboard-startup-banner 'logo)
                   `(initial-buffer-choice (lambda () (get-buffer ,dashboard-name))))
                  (global-set-key (kbd "C-c d") (lambda ()
                                                  (interactive)
                                                  (switch-to-buffer dashboard-name)
                                                  (delete-other-windows)))))

;; ivy
(enable-package 'ivy nil
                (ivy-mode)
                (custom-set-variables
                 `(ivy-use-virtual-buffers t)
                 `(enable-recursive-minibuffers t))
                ;;`(search-default-mode #'char-fold-to-regexp))
                (global-set-key "\C-s" 'swiper)
                (global-set-key (kbd "C-c C-r") 'ivy-resume)
                (global-set-key (kbd "<f6>") 'ivy-resume)
                (global-set-key (kbd "M-x") 'counsel-M-x)
                (global-set-key (kbd "C-x C-f") 'counsel-find-file)
                (global-set-key (kbd "<f1> f") 'counsel-describe-function)
                (global-set-key (kbd "<f1> v") 'counsel-describe-variable)
                (global-set-key (kbd "<f1> o") 'counsel-describe-symbol)
                (global-set-key (kbd "<f1> l") 'counsel-find-library)
                (global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
                (global-set-key (kbd "<f2> u") 'counsel-unicode-char)
                (global-set-key (kbd "C-c g") 'counsel-git)
                (global-set-key (kbd "C-c j") 'counsel-git-grep)
                (global-set-key (kbd "C-c k") 'counsel-ag)
                (global-set-key (kbd "C-x l") 'counsel-locate)
                (global-set-key (kbd "C-S-o") 'counsel-rhythmbox)
                (define-key minibuffer-local-map (kbd "C-r") 'counsel-minibuffer-history)
                ;; hack to fix eshell binary completion
                (add-hook 'eshell-mode-hook
                          (lambda ()
                            (setq eshell-cmpl-cycle-completions nil)
                            (define-key eshell-mode-map (kbd "<tab>")
                              #'(lambda ()
                                  (interactive)
                                  (pcomplete t))))))

;; all-the-icons-ivy
(enable-package 'all-the-icons-ivy nil
                (all-the-icons-ivy-setup))

;; all-the-icons-ivy-rich
(enable-package 'all-the-icons-ivy-rich nil
                (all-the-icons-ivy-rich-mode 1))

;; expand-region
(enable-package 'expand-region nil
                (global-set-key (kbd "C-=") 'er/expand-region))

;; persistent-scratch
(enable-package 'persistent-scratch nil
                (persistent-scratch-setup-default))

;; projectile
(enable-package 'projectile nil
                (projectile-mode +1)
                (eval-when-compile
                  (require 'projectile)
                  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)))

;; doom-modeline
(enable-package 'doom-modeline nil
                ;; hack for fixing fast-set-tags display
                ;; when using doom-modeline
                (advice-add #'fit-window-to-buffer :before
                            (lambda (&rest _r) (redisplay t)))
                (doom-modeline-mode 1))

;; doom-themes
(enable-package 'doom-themes nil
                (custom-set-variables
                 `(doom-themes-enable-bold t)
                 `(doom-themes-enable-italic t))
                ;; flashing mode-line on errors
                (doom-themes-visual-bell-config)
                ;; custom neotree theme
                (doom-themes-neotree-config)
                ;; improves org-mode's native fontification.
                (doom-themes-org-config)
                ;; load-theme
                (load-theme `doom-dracula t))

;; all-the-icons
(enable-package 'all-the-icons)

;; neotree
(enable-package 'neotree nil
                (global-set-key [f9] 'neotree-toggle)
                ;; enable icons when emacs is in gui mode
                ;;     - requires /all-the-icons/ package
                ;;     - M-x all-the-icons-install-fonts
                (when (display-graphic-p)
                  (custom-set-variables `(neo-theme 'icons))))

;; undo-tree
;;   use `M-x undo-tree-mode` to enable
;;   enabling global mode breaks things for some reason with this package
;; TODO: need to define undo-tree-history-directory-alist
(enable-package 'undo-tree nil
                (require 'undo-tree))

;; undo-fu
(enable-package 'undo-fu-session nil
                (require 'undo-fu-session)
                (global-undo-fu-session-mode))

;; latex-preview-pane
(enable-package 'latex-preview-pane-mode t
                (add-to-list 'auto-mode-alist '("\\.tex\\'" . latex-preview-pane-mode)))

;; nyan-mode
(enable-package 'nyan-mode nil
                (nyan-mode 1))

;; async
(enable-package 'async nil
                (async-bytecomp-package-mode 1))

;; markdown-mode
(enable-package 'markdown-mode nil
                (autoload 'markdown-mode "markdown-mode"
                  "major mode for editing md files" t)
                (add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
                (add-to-list 'auto-mode-alist '("\\.md\\'" . gfm-mode))
                (add-to-list 'auto-mode-alist '("readme\\.md\\'" . gfm-mode))
                (autoload 'gfm-mode "markdown-mode"
                  "major mode for editing github md files" t)
                (custom-set-variables
                 `(markdown-command "~/.npm-packages/bin/marked --gfm --breaks --smart-lists")))

;; dminish
(enable-package 'diminish nil
                (diminish 'projectile-mode "pj")
                (diminish 'markdown-mode "md")
                (diminish 'auto-complete-mode)
                (diminish 'auto-revert-mode)
                (diminish 'ivy-mode)
                (diminish 'eldoc-mode)
                (diminish 'flycheck-mode))

;; flyspell
(enable-package 'flyspell)

;; flyspell-correct-ivy
(enable-package 'flyspell-correct-ivy nil
                (eval-when-compile
                  (require 'flyspell)
                  (define-key flyspell-mode-map (kbd "C-;") 'flyspell-correct-wrapper)))

;; flycheck
(enable-package 'flycheck nil
                (global-flycheck-mode 1)
                (custom-set-variables
                 `(flycheck-clang-include-path ',dev-path)
                 `(flycheck-gcc-include-path ',dev-path)
                 `(flycheck-disabled-checkers '(emacs-lisp-checkdoc))))

;; flymake-shell
(enable-package 'flymake-shell nil
                (add-hook 'sh-set-shell-hook 'flymake-shell-load))

;; flymake-shellcheck
;; use system package manager to install shellcheck itself
(enable-package 'flymake-shellcheck-load nil
                (add-hook 'sh-mode-hook 'flymake-shellcheck-load))

;; vala-mode
(enable-package 'vala-mode)

;; meson-mode
(enable-package 'meson-mode)

;; magit
(enable-package 'magit)

;; auto-complete
(enable-package 'auto-complete nil
                (ac-config-default))

;; ac-sly
(enable-package 'ac-sly nil
                (eval-when-compile
                  (defvar ac-modes))
                (add-hook 'sly-mode-hook 'set-up-sly-ac)
                (eval-after-load 'auto-complete
                  '(add-to-list 'ac-modes 'sly-mrepl-mode)))

;; sly
(enable-package 'sly nil
                (custom-set-variables `(inferior-lisp-program "sbcl")))

;; ac-clang
;; the instructions for clang-server are pretty annoying, but also unecessary
;; depending on the version of llvm your system package manager ships with.
;; install llvm, clang, and clang-libs through your system package manager.
;; my only issue was needing to add "/usr/lib" to libclang_search_path in
;; CMakeLists.txt inside the .../elpa/ac-clang-.../clang-server directory
;; then set clang-server--executable location with custom-set-variables
;; i may setup irony-mode and company-irony instead later on
(enable-package 'ac-clang t
                (eval-when-compile
                  (require 'ac-clang)
                  (custom-set-variables
                   `(clang-server--executable "~/.local/bin/clang-server"))
                  (ac-clang-initialize)
                  (add-hook 'c-mode-common-hook 'ac-clang-activate)))

;; glsl-mode
(enable-package 'glsl-mode nil
                (add-to-list 'auto-mode-alist '("\\.glsl\\'" . glsl-mode))
                (add-to-list 'auto-mode-alist '("\\.vert\\'" . glsl-mode))
                (add-to-list 'auto-mode-alist '("\\.frag\\'" . glsl-mode))
                (add-to-list 'auto-mode-alist '("\\.geom\\'" . glsl-mode)))

;; rtags
(enable-package 'rtags)

;; cmake-mode
(enable-package 'cmake-mode)

;; cmake-ide
(enable-package 'cmake-ide nil
                (require 'rtags)
                (cmake-ide-setup))

;; org-view-mode
(enable-package 'org-view-mode)

;; org-modern
(enable-package 'org-modern)
                ;; (add-hook 'org-mode-hook 'org-modern-mode))

;; org-roam
(enable-package 'org-roam nil
                (let ((roam-dir (file-truename "~/.emacs.d/org-roam/")))
                  (custom-set-variables
                   `(org-roam-directory ,roam-dir)
                   `(org-roam-db-location (concat ,roam-dir "org-roam.db"))))
                (org-roam-db-autosync-mode)
                (global-set-key (kbd "C-c n l") 'org-roam-buffer-toggle)
                (global-set-key (kbd "C-c n f") 'org-roam-node-find)
                (global-set-key (kbd "C-c n g") 'org-roam-graph)
                (global-set-key (kbd "C-c n i") 'org-roam-node-insert)
                (global-set-key (kbd "C-c n o") 'org-roam-node-open)
                (global-set-key (kbd "C-c n c") 'org-roam-capture)
                (global-set-key (kbd "C-c n j") 'org-roam-dailies-capture-today)
                (require 'org-roam-protocol))

;; org-roam-ui
(enable-package 'org-roam-ui nil
                (custom-set-variables
                 `(org-roam-ui-sync-theme t)
                 `(org-roam-ui-follow t)
                 `(org-roam-ui-update-on-save t)
                 `(org-roam-ui-open-on-start nil))
                (global-set-key (kbd "C-c n r") 'org-roam-ui-open)
                (global-set-key (kbd "C-c n u") 'org-roam-ui-mode))

;; deft
(enable-package 'deft nil
                (custom-set-variables
                 `(deft-recursive t)
                 `(deft-use-filename-as-title t)
                 `(deft-use-filter-string-for-filename t)
                 `(deft-default-extension "org")
                 `(deft-directory org-roam-directory))
                (global-set-key (kbd "C-c n d") 'deft))

;; mini-frame
(enable-package 'mini-frame nil
                ;;(mini-frame-mode 1)
                (custom-set-variables
                 ;;`(mini-frame-standalone t)
                 `(mini-frame-color-shift-step 15)
                 `(mini-frame-show-parameters
                   '((top . 0.7)
                     (width . 0.5)
                     (left . 0.5)
                     (height . 10)))))

;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; custom packages ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;

;; debug-buffer
(enable-package 'debug-buffer)

;; calculate-lisp-indent
(enable-package 'calculate-lisp-indent)

;; b-mode
(enable-package 'b-mode t
                (b-mode-global 1)
                (b-mode-set-key-binding "C-S-b")
                (b-mode-global-set-key-binding "C-c C-b")
                (b-mode-set-exclude-autoloads t)
                (b-mode-add-excluded-files
                 '(".git/COMMIT_EDITMSG"))
                (b-mode-add-exclude-major-modes
                 'auto-complete-mode 'sly-mrepl-mode 'latex-preview-pane-mode
                 'magit-status-mode 'magit-mode 'dashboard-mode 'org-mode
                 'org-agenda-mode 'org-roam-mode)
                (b-mode-set-window-resize-delta 5))

;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; custom set faces ;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;

(custom-set-faces
 ;; buffer space at top of emacs window (inherit bg color)
 `(header-line ((t (:inherit t :box nil)))))
